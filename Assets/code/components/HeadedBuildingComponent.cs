﻿using UnityEngine;
using System.Collections;

public class HeadedBuildingComponent : BuildingComponent {

	GameObject head;
	Vector2 headDirection;

	protected override void SetCompleteState() {
		base.SetCompleteState();
		head = body.transform.Find("Head").gameObject;
	}

	protected virtual void UpdateHead() {
		if (unit.Tile.fowState == FoWState.Visible && head != null) {
			float step =  Mathf.Min(1.0f, 10.0f * Time.deltaTime);
			headDirection += (unit.headDirection - headDirection) * step;
			head.transform.eulerAngles = new Vector3(0, Mathf.Atan2(headDirection.y, -headDirection.x) * Mathf.Rad2Deg, 0);
		}
	}
	
	void Update () {
		UpdateBuilding();
		UpdateHead();
		UpdateGameObject();
	}
}
