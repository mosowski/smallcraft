﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public static class Actions {

	private static Hashtable QueueMyAction() {
		Hashtable action = new Hashtable();
		action["player"] = Game.me.name;
		action["tickNum"] = Game.currentTickNum + 5;
		return action;
	}

	public static void QueueActionSelect(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 v4) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionSelect";
		action["x1"] = (double)v1.x;
		action["y1"] = (double)v1.y;
		action["x2"] = (double)v2.x;
		action["y2"] = (double)v2.y;
		action["x3"] = (double)v3.x;
		action["y3"] = (double)v3.y;
		action["x4"] = (double)v4.x;
		action["y4"] = (double)v4.y;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionMsg(string msg) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionMsg";
		action["msg"] = msg;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionMove(Vector2 v) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionMove";
		action["x"] = (double)v.x;
		action["y"] = (double)v.y;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionAttack(Vector2 v) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionAttack";
		action["x"] = (double)v.x;
		action["y"] = (double)v.y;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionAttackMove(Vector2 v) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionAttackMove";
		action["x"] = (double)v.x;
		action["y"] = (double)v.y;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionGather(Vector2 v) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionGather";
		action["x"] = (double)v.x;
		action["y"] = (double)v.y;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionSelectClick(Vector2 v) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionSelectClick";
		action["x"] = (double)v.x;
		action["y"] = (double)v.y;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionStop() {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionStop";
		Game.queuedActions.Add(action);
	}

	public static void QueueActionQueueProductionOrder(string itemName) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionProduceOrder";
		action["itemName"] = itemName;
		Game.queuedActions.Add(action);
	}

	public static void QueueActionPlaceBuilding(string modelId, int col, int row) {
		Hashtable action = QueueMyAction();
		action["type"] = "ActionPlaceBuilding";
		action["modelId"] = modelId;
		action["col"] = col;
		action["row"] = row;
		Game.queuedActions.Add(action);
	}

	public static void PerformAction(JsonData action) {
		Player player = Game.GetPlayer((string)action["player"]);
		if ((string)action["type"] == "ActionSelect") {
			PerformActionSelect(player, action);
		} else if ((string)action["type"] == "ActionSelectClick") {
			PerformActionSelectClick(player, action);
		} else if ((string)action["type"] == "ActionMove") {
			PerformActionMove(player, action);
		} else if ((string)action["type"] == "ActionAttack") {
			PerformActionAttack(player, action);
		} else if ((string)action["type"] == "ActionAttackMove") {
			PerformActionAttackMove(player, action);
		} else if ((string)action["type"] == "ActionProduceOrder") {
			PerformActionProduceOrder(player, action);
		} else if ((string)action["type"] == "ActionStop") {
			PerformActionStop(player, action);
		} else if ((string)action["type"] == "ActionGather") {
			PerformActionGather(player, action);
		} else if ((string)action["type"] == "ActionPlaceBuilding") {
			PerformActionPlaceBuilding(player, action);
		} else if ((string)action["type"] == "ActionMsg") {
			PerformActionMsg(player, action);
		}
	}

	private static void PerformActionMsg(Player player, JsonData action) {
		string msg = (string)action["msg"];
		if (!Cheats.ProcessCheat(player, msg)) {
			Notifications.Add(player.name + ": " + (string)action["msg"], 20f);
		} else {
			Notifications.Add(player.name + ": gl hf :)");
		}

	}

	private static void PerformActionSelect(Player player, JsonData action) {
	 	player.Select(
	 		new Vector2((float)(double)action["x1"], (float)(double)action["y1"]),
	 		new Vector2((float)(double)action["x2"], (float)(double)action["y2"]),
	 		new Vector2((float)(double)action["x3"], (float)(double)action["y3"]),
	 		new Vector2((float)(double)action["x4"], (float)(double)action["y4"])
	 	);
		Game.AcknowledgeGroupOrdered(player.selection);
	}

	private static void PerformActionSelectClick(Player player, JsonData action) {
		player.ClickSelect(new Vector2((float)(double)action["x"], (float)(double)action["y"]));
	}

	private static void PerformActionStop(Player player, JsonData action) {
		foreach (Unit unit in player.selection) {
			unit.SetOrder(null);
		}
	}

	private static void PerformActionMove(Player player, JsonData action) {
		float clickX = (float)(double)action["x"];
		float clickY = (float)(double)action["y"];
		MoveOrder.SetToGroup<MoveOrder>(player.selection, new Vector2(clickX, clickY));
		Game.AcknowledgeGroupOrdered(player.selection);
	}

	private static void PerformActionAttackMove(Player player, JsonData action) {
		float clickX = (float)(double)action["x"];
		float clickY = (float)(double)action["y"];
		MoveOrder.SetToGroup<AttackMoveOrder>(player.selection, new Vector2(clickX, clickY));
		Game.AcknowledgeGroupOrdered(player.selection);
	}

	private static void PerformActionGather(Player player, JsonData action) {
		float clickX = (float)(double)action["x"];
		float clickY = (float)(double)action["y"];
		foreach (Unit u in player.selection) {
			u.SetOrder(new GatherOrder(u, new Vector2(clickX, clickY)));
		}
		Game.AcknowledgeGroupOrdered(player.selection);
	}

	private static void PerformActionAttack(Player player, JsonData action) {
		float clickX = (float)(double)action["x"];
		float clickY = (float)(double)action["y"];
		Unit targetUnit = Game.GetNearestUnit(new Vector2(clickX, clickY));
		if (targetUnit != null && !targetUnit.model.isIndestructible) {
			foreach (Unit u in player.selection) {
				AttackFeature attackFeature = u.GetFeature<AttackFeature>();
				if (attackFeature != null) {
					attackFeature.targetUnit = targetUnit;
				}
			}
			MoveOrder.SetToGroup<AttackOrder>(player.selection, targetUnit.position);
			Game.AcknowledgeGroupOrdered(player.selection);
		}
	}

	private static void PerformActionProduceOrder(Player player, JsonData action) {
		if (player.selection.Count > 0) {
			string itemName = (string)action["itemName"];
			UnitModel uModel = Game.me.model.GetModel(itemName);
			if (player.gold < uModel.cost || player.usedSupply + uModel.supplyCost > player.providedSupply) {
				//TODO: Notify player
				return;
			}
			foreach (Unit u in player.selection) {
				u.SetOrder(new QueueProductionOrder(u, itemName));
				player.gold -= uModel.cost;
			}

			Game.AcknowledgeGroupOrdered(player.selection);
		}
	}

	private static void PerformActionPlaceBuilding(Player player, JsonData action) {
		string modelId = (string)action["modelId"];
		int col = (int)action["col"];
		int row = (int)action["row"];
		if (player.selection.Count > 0) {
			Unit u = player.selection[0];
			u.SetOrder(new ConstructOrder(u, modelId, col, row));
			Game.AcknowledgeGroupOrdered(player.selection);
		}
	}
}
