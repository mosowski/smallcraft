using System.Collections;
using UnityEngine;

public class MusicManager {

	private static GameObject music;

	public static void PlayGame() {
		Stop();
		music = AudioManager.PlaySFX("scBgMusic", Camera.main.transform.position, true);
	}

	public static void PlayMenu() {
		Stop();
		music = AudioManager.PlaySFX("menuBgMusic", Camera.main.transform.position, true);
	}

	public static void Stop() {
		if (music != null) {
			AudioManager.ReleaseSFX(music);
			music = null;
		}
	}
}
