﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Point {
	public int x;
	public int y;
}

public static class Bresenham {

	

	private static Point point = new Point();

	public static IEnumerable<Point> Line(int x1, int y1, int x2, int y2) {
		int dx = x2 - x1;
		int dy = y2 - y1;
		int sx = dx > 0 ? 1 : -1;
		int sy = dy > 0 ? 1 : -1;
		int e = 0;
		dx *= sx;
		dy *= sy;

		point.x = x1;
		point.y = y1;
		yield return point;

		if (dx > dy) {
			e = 2*dy - dx;
			while (x1 != x2) {
				x1 += sx;
				if (e > 0) {
					e -= 2*dx;
					y1 += sy;
				}
				e += 2*dy;
				point.x = x1;
				point.y = y1;
				yield return point;
			}
		} else {
			e = 2*dx - dy;
			while (y1 != y2) {
				y1 += sy;
				if (e > 0) {
					e -= 2*dy;
					x1 += sx;
				}
				e += 2*dx;
				point.x = x1;
				point.y = y1;
				yield return point;
			}
		}
	}
}
