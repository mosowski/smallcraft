﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public static class Game {
	public static bool isPlaying;
	public static int mapSize = 128;
	public static MapTile[,] map;
	public static List<Player> players;
	public static List<Unit> units;
	public static List<DelayedDamage> delayedDamages;

	public static List<JsonData> actions;
	public static List<Hashtable> queuedActions;
	public static int currentTickNum = 0;
	public static int localTickNum = 0;

	public static Player me;
	public static FogOfWar fow;
	public static Player neutral;

	public static List<Player> playersDown = new List<Player>();

	public static int playersCount = 0;

	public static void Init(List<string> playerNames, string myName) {
		isPlaying = true;
		Notifications.Init();
		units = new List<Unit>();
		map = MapReader.GetObstaclesMap(128,128);
		players = new List<Player>();
		delayedDamages = new List<DelayedDamage>();
		playersCount = playerNames.Count;
		for (int i = 0; i < playerNames.Count; ++i) {
			Vector2 startingLocation = Vector3To2(GameObject.Find("StartingLocation" + (i+1)).transform.position);
			if (myName == playerNames[i]) {
				CameraMover.LookAt(startingLocation);
			}
			Player p = InitPlayer(playerNames[i], i, startingLocation);
			p.isHuman = true;
			players.Add(p);
		}
		//players.Add(InitPlayer("p2", 2, Vector3To2(GameObject.Find("StartingLocation2").transform.position)));

		neutral = InitPlayer("__neural__", 404);
		players.Add(neutral);

		InitMinerals();

		me = GetPlayer(myName);
		fow = new FogOfWar();
		actions = new List<JsonData>();
		queuedActions = new List<Hashtable>();
	}

	public static void InitMinerals() {
		foreach (Vector2 position in MapReader.GetMinerals()) {
			Factory.MakeMineral(position);
		}
	}

	public static void PlayerLost(Player p) {
		playersDown.Add(p);
		if (p == Game.me) {
			Notifications.Add("You lost");
			Root.StopGame();
		} else {
			Notifications.Add("Player " + p.name + " just got facerolled");
		}
		CheckGameCondition();
		
	}

	public static void PlayerDisconnected(Player p) {
		playersDown.Add(p);
		Notifications.Add("Player " + p.name + " has left the game.");
		CheckGameCondition();
	}

	public static void CheckGameCondition() {
		if(playersCount - playersDown.Count == 1 && playersDown.IndexOf(Game.me) == -1) {
			Notifications.Add("You won, gratz!");
			Root.ReportMatchResult(new List<string>() { Game.me.name }, playersDown.Select(player => player.name).ToList());
			Root.StopGame();
		}
	}

	public static Player GetPlayer(string name) {
		for (int i = 0; i < players.Count; ++i) {
			if (players[i].name == name) {
				return players[i];
			}
		}
		return null;
	}

	public static void Clear() {
		units.ForEach(unit => Object.Destroy(unit.gameObject));
	}

	public static void Tick() {
		DoActions();
		currentTickNum++;
		localTickNum = currentTickNum % 5;

		for (int i = 0; i < units.Count; ++i) {
			units[i].Tick();
		}

		TickDelayedDamages();

		for (int i = 0; i < units.Count; ++i) {
			if (units[i].isDead) {
				units[i].Remove();
			}
		}
		units.RemoveAll(unit => unit.isDead);

		players.ForEach(player => player.Tick());
		fow.Tick();

	}

	private static void TickDelayedDamages() {
		for (int i = 0; i < delayedDamages.Count;) {
			delayedDamages[i].timeLeft--;
			if (delayedDamages[i].timeLeft <= 0) {
				delayedDamages[i].target.Damage(delayedDamages[i].amount);
				delayedDamages[i] = delayedDamages[delayedDamages.Count - 1];
				delayedDamages.RemoveAt(delayedDamages.Count - 1);
			} else {
				i++;
			}
		}
	}

	private static void DoActions() {
		for (int i = 0; i < actions.Count; ++i) {
			if ((int)(actions[i]["tickNum"]) == currentTickNum) {
				Actions.PerformAction(actions[i]);
			}
		}
		for (int i = actions.Count - 1; i >= 0; --i) {
			if ((int)(actions[i]["tickNum"]) == currentTickNum) {
				actions.RemoveAt(i);
			}
		}
	}

	public static Player InitPlayer(string name, int number) {
		Player player = new Player();
		player.name = name;
		player.number = number;
		return player;
	}

	public static Player InitPlayer(string name, int number, Vector2 startingLocation) {
		Player player = InitPlayer(name, number);
		player.BuildStartingBase(startingLocation);
		return player;
	}

	public static void AddUnit(Unit unit) {
		units.Add(unit);
		unit.Tile.units.Add(unit);
		if (unit is Building) {
			((Building)unit).PlaceOnMap();
		}
	}

	public static bool IsEnemy(Player a, Player b) {
		return a != b && a != neutral && b != neutral;
	}

	public static float GetMapHeight(Vector2 p) {
		return GetMapTile(p).GetHeight(p.x - (int)p.x, p.y - (int)p.y);
	}

	public static MapTile GetMapTile(Vector2 p) {
		return map[(int)p.x + 1, (int)p.y + 1];
	}

	public static MapTile GetMapTile(int col, int row) {
		return map[col,row];
	}

	public static MapTile SafeGetMapTile(Vector2 p) {
		int col = (int)p.x + 1, row = (int)p.y + 1;
		return SafeGetMapTile(col, row);
	}

	public static MapTile SafeGetMapTile(int col, int row) {
		if (col >= 0 && row >= 0 && col < 128 && row < 128) {
			return map[col, row];
		}
		return null;
	}

	public static bool IsWalkable(MapTile mapTile) {
		return mapTile.extraCost < 1000;
	}

	public static bool IsWalkable(Vector2 p) {
		return IsWalkable(GetMapTile(p));
	}

	public static bool IsStraightPathWalkable(Vector2 a, Vector2 b) {
		return IsStraightPathWalkable(GetMapTile(a), GetMapTile(b));
	}

	public static bool IsStraightPathWalkable(MapTile a, MapTile b) {
		if (a.x == b.x && a.y == b.y) {
			return true;
		}
		bool isOk = true;
		//Debug.Log("is walkable " + a.x + " " + a.y + " " + b.x + " " + b.y);
		IEnumerable<Point> tmp = Bresenham.Line(a.x, a.y, b.x, b.y);
		foreach(Point p in tmp) {
			isOk = isOk && Game.IsWalkable(Game.GetMapTile(p.x, p.y));
		}
		//Debug.Log(isOk);
		return isOk;
	}

	public static bool IsInGameArea(Vector2 p) {
		return p.x >= 0 && p.y >= 0 && p.x < mapSize && p.y < mapSize;
	}

	public static IEnumerable<Unit>GetUnitsNear(Vector2 p) {
		return GetUnitsInRadius(p, 3);
	}

	public static IEnumerable<Unit>GetUnitsInRadius(Vector2 p, int radius) {
		MapTile c = SafeGetMapTile(p);
		if (c != null) {
			int left = ClampToBounds(c.x - radius);
			int bottom = ClampToBounds(c.y - radius);
			int right = ClampToBounds(c.x + radius);
			int top = ClampToBounds(c.y + radius);
			for (int i = left; i <= right; ++i) {
				for (int j = bottom; j <= top; ++j) {
					foreach (Unit unit in map[i,j].units) {
						yield return unit;
					}
				}
			}
		}
	}

	public static Unit GetNearestUnit(Vector2 v) {
		Unit nearest = null;
		float nearestDist = 0;
		foreach (Unit unit in Game.GetUnitsNear(v)) {
			float newDist = Vector2.Distance(unit.position, v);
			if (nearest == null || newDist < nearestDist) {
				if (newDist < unit.model.clickRadius) {
					nearest = unit;
					nearestDist = newDist;
				}
			}
		}
		return nearest;
	}	

	public static bool CanPlaceBuilding(Vector2 v, UnitModel model) {
		int anchorCol, anchorRow;
		model.GetMapCoords(v, out anchorCol, out anchorRow);

		return CanPlaceBuilding(anchorCol, anchorRow, model);
	}

	public static bool CanPlaceBuilding(int anchorCol, int anchorRow, UnitModel model) {
		for (int i = anchorCol; i < anchorCol + model.tileWidth; ++i) {
			for (int j = anchorRow; j < anchorRow + model.tileHeight; ++j) {
				MapTile tile = GetMapTile(i, j);
				if (!IsWalkable(tile)) {// || tile.units.Count != 0) {
					return false;
				}
			}
		}
		return true;
	}

	

	public static IEnumerable<MapTile> GetTilesInRadialOrder(Vector2 v, int radius) {
		MapTile t = GetMapTile(v);
		int x = t.x;
		int y = t.y;
		int l = 1, s = 1;
		int numSteps = 4*radius*radius;
		while (numSteps >= 2*l) {
			for (int i = 0; i < l; ++i) {
				MapTile tile = SafeGetMapTile(x, y);
				if (tile != null) {
					yield return tile;
				}
				x += s;
			}
			for (int i = 0; i < l; ++i) {
				MapTile tile = SafeGetMapTile(x, y);
				if (tile != null) {
					yield return tile;
				}
				y += s;
			}
			s = -s;
			numSteps -= 2 * l;
			l++;
		}
	}

	public static MapTile GetNearestWalkableTile(Vector2 v, int radius) {
		foreach (MapTile tile in GetTilesInRadialOrder(v, radius)) {
			if (IsWalkable(tile)) {
				return tile;
			}
		}
		return null;
	}

	public static MapTile GetNearestWalkableTileToPoint(Vector2 v, int radius, Vector2 p) {
		MapTile nearest = null;
		float sqNearestDist = 0;
		foreach (MapTile tile in GetTilesInRadialOrder(v, radius)) {
			if (IsWalkable(tile)) {
				float newSqDist = (tile.x-0.5f - p.x) * (tile.x-0.5f - p.x) + (tile.y-0.5f - p.y) * (tile.y-0.5f - p.y); 
				if (nearest == null || newSqDist < sqNearestDist) {
					nearest = tile;
					sqNearestDist = newSqDist;
				}
			}
		}
		return nearest;
	}

	public static Unit GetNearestDepositUnit(Player player, Vector2 p) {
		Unit nearest = null;
		float sqNearestDist = 0;
		foreach (Unit u in player.units) {
			if (u is Building && u.model.id == "CommandCenter" && u.IsComplete) {
				float sqDist = (u.position.x - p.x) * (u.position.x - p.x) + (u.position.y - p.y) * (u.position.y - p.y);
				if (nearest == null || sqDist < sqNearestDist) {
					nearest = u;
					sqNearestDist = sqDist;
				}
			}
		}
		return nearest;
	}

	public static MapTile GetNearestDepositTile(Unit unit, Vector2 p) {
		//XXX
		return Game.GetNearestWalkableTileToPoint(unit.position - new Vector2(0.1f, 0.1f), 3, p);
	}

	public static Mineral GetNearestMineral(Vector2 v) {
		foreach (MapTile tile in GetTilesInRadialOrder(v, 6)) {
			if (tile.Minerals > 0) {
				return tile.FirstMineral;
			}
		}
		return null;
	}

	public static Vector2 GetTilePosition(MapTile tile) {
		return new Vector2(tile.x - 0.5f, tile.y - 0.5f);
	}

	public static Vector2 GetTilePosition(int column, int row) {
		return new Vector2(column - 0.5f, row - 0.5f);
	}

	public static void DealSplashDamage(Vector2 p, float radius, int damage, int delay) {
		foreach (Unit u in GetUnitsInRadius(p, (int)(radius + 1))) {
			float distance = Vector2.Distance(u.position, p);
			if (distance <= radius) {
				int tieredDamage = (int)((1.0f - distance/radius) * damage);
				DealDamage(u, tieredDamage, delay);
			}
		}
	}

	public static void DealDamage(Unit unit, int amount, int delay) {
		if (delay == 0) {
			unit.Damage(amount);
		} else {
			delayedDamages.Add(new DelayedDamage() {
				target = unit,
				amount = amount,
				timeLeft = delay
			});
		}
	}

	public static int ClampToBounds(int c) {
		if (c < 0) {
			return 0;
		} else if (c > 127) {
			return 127;
		} else {
			return c;
		}
	}

	public static Vector2 Vector3To2(Vector3 v) {
		return new Vector2(v.x, v.z);
	}

	public static void DebugLine(Vector2 a, Vector2 b, Color c, float d = 10.0f) {
		Debug.DrawRay(new Vector3(a.x, 1.5f, a.y), new Vector3(b.x-a.x, 0.0f, b.y-a.y), c, d);
	}

	public static void AcknowledgeUnitConstructed(Player player, Unit unit) {
		if (player == Game.me) {
			Notifications.Add(unit.model.displayName + " ready");
			unit.PlayAcknowledgeSound();
		}
	}

	public static void AcknowledgeGroupOrdered(List<Unit> group) {
		if (group.Count > 0 && group[0].player == Game.me) {
			group[0].PlayAcknowledgeSound();
		}
	}
}
