﻿using UnityEngine;
using System.Collections;

public class Mineral : Unit {
	public int minerals;
	private int numGatheringWorkers;

	public override void Tick() {
		if (minerals <= 0) {
			isDead = true;
		}
	}

	public bool CanGather {
		get {
			return numGatheringWorkers < 3; 
		}
	}

	public void AddWorker() {
		numGatheringWorkers++;
	}

	public void RemoveWorker() {
		numGatheringWorkers--;
	}
}
