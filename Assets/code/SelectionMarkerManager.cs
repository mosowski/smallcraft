﻿using UnityEngine;
using System.Collections;

public static class SelectionMarkerManager {

	public class SelectionMarkerManagerPool : Pool {
		public override GameObject Construct(string prefabId) {
			GameObject obj = Object.Instantiate(Resources.Load<GameObject>("misc/SelectionCircle")) as GameObject;
			return obj;
		}
	}

	public static SelectionMarkerManagerPool pool = new SelectionMarkerManagerPool();

	public static GameObject CreateSelectionMarker(Unit unit) {
		GameObject obj = pool.Pick("SelectionCircle");
		obj.renderer.material = MaterialManager.GetMaterialForPlayer(obj.renderer.material, unit.player);
		obj.transform.position = new Vector3(unit.position.x, unit.Tile.height, unit.position.y);
		obj.transform.localScale = new Vector3(unit.model.clickRadius, 1, unit.model.clickRadius);
		return obj;
	}

	public static void ReleaseSelectionMarker(GameObject obj) {
		pool.Release(obj);
	}
}
