﻿using UnityEngine;
using System.Collections;

public class AnimatedUnitComponent : UnitComponent {

	protected Animator animator; 
	protected int lastShootTick;

	AttackFeature attackFeature;

	protected virtual void UpdateAnimation() {
		if (!unit.isDead) {
			int animationState = 0;
			animator.speed = 1.0f;
			if (attackFeature != null && attackFeature.HasShot && lastShootTick != Game.currentTickNum) {
				animator.SetTrigger("shoot");
				lastShootTick = Game.currentTickNum;
			} else if (unit.IsMoving) {
				animationState = 1;
				animator.speed = 2.0f;
			} else if (unit.IsGathering) {
				animationState = 3;
			}
			animator.SetInteger("animationState", animationState);
		}
	}

	public override void Setup(Unit unit) {
		base.Setup(unit);

		SetPositionImmediately();
		attackFeature = unit.GetFeature<AttackFeature>();
		animator = body.GetComponent<Animator>();
		IsVisible = false;
	}
	
	void Update () {
		UpdateSelectionMarker();
		UpdateUnit();
		UpdateAnimation();
		UpdateGameObject();
	}
}
