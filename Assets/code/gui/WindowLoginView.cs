﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindowLoginView : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Lobby.LoginFailed += OnLoginFailed;
		Lobby.LoginOk += OnLoginOk;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Return)) {
			AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
			string login = transform.Find("LoginInput").GetChild(0).GetComponent<UIInput>().value;
			string password = transform.Find("PasswordInput").GetChild(0).GetComponent<UIInput>().value;
			Lobby.LogIn(login, password);
		}
	}

	void OnLoginFailed(string error) {
		transform.Find("Status").GetComponent<UILabel>().text = "Login failed:" + error;
	}

	void Close() {
		Lobby.LoginFailed -= OnLoginFailed;
		Lobby.LoginOk -= OnLoginOk;
		GUIManager.CloseWindow("WindowLogin");
	}

	void OnLoginOk() {
		GUIManager.OpenWindow("WindowRooms");
		Close();		
	}
}