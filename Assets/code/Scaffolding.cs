﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Scaffolding : Unit {

	public int anchorCol;
	public int anchorRow;
	public Vector2 rallyOffset;

	public override void Setup(UnitModel model) {
		base.Setup(model);
	}

	public void PlaceOnMap() {
		for (int i = anchorCol; i < anchorCol + model.tileWidth; ++i) {
			for (int j = anchorRow; j < anchorRow + model.tileHeight; ++j) {
				Game.GetMapTile(i, j).extraCost += 1000;
			}
		}
	}

	public void RemoveFromMap() {
		for (int i = anchorCol; i < anchorCol + model.tileWidth; ++i) {
			for (int j = anchorRow; j < anchorRow + model.tileHeight; ++j) {
				Game.GetMapTile(i, j).extraCost -= 1000;
			}
		}
	}

	public override void Tick() {
		base.Tick();
	}

	public override MapTile Tile {
		get { return Game.GetMapTile(anchorCol, anchorRow); }
	}

	public override void Remove() {
		RemoveFromMap();
		base.Remove();
	}
}
