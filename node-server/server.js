var net = require('net');
var fs = require('fs');
var redis = require('redis');
var crypto = require('crypto');
var _ = require('underscore');

var maxInRoom = 4;
var listenPort = 4242;
var crossdomainPort = -1;
var redisDb = 777;

if (process.argv.length > 2) {
	listenPort = parseInt(process.argv[2]);
}
if (process.argv.length > 3) {
	maxInRoom = parseInt(process.argv[3]);
}
if (process.argv.length > 4) {
	crossdomainPort = parseInt(process.argv[4]);
}
if (process.argv.length > 5) {
	redisDb = parseInt(process.argv[5]);
}

function getSHA1(str) {
	var generator = crypto.createHash('sha1');
	generator.update(str);
	return generator.digest('hex');
}

function prepareMsg(method, data) {
	return JSON.stringify({
		m: method,
		args: data
	});
}

function Server() {
	this.numTurn = 0;
	this.rooms = {};
	this.numPlayersOnline = 0;
	this.playersByName = {};
	this.isStarted = false;
}

function Room(roomName, ownerName) {
	this.name = roomName;
	this.ownerName = ownerName;
	this.members = [];
	this.max = maxInRoom;
	this.numTurn = 0;
	this.cmds = [];
	this.isStarted = false;
	this.hasMatchEnded = false;
}

function Player(socket) {
	this.socket = socket;
	this.numTurn = 1;
	this.roomName = null;
	this.cmd = false;
	this.eloRank = 0;
	this.name = '';
	this.isOnline = false;
}

function Actions(num_of_turn, action) {
	this.numTurn = num_of_turn;
	this.turnActions = action;
}

Room.prototype.start = function() {
	if (!this.isStarted) {
		this.isStarted = (this.members.length > 0);

		if (this.isStarted) {
			this.numTurn++;
			var playerNames = [];
			_.each(this.members, function (player) {
				playerNames.push(player.name);
			});
			this.broadcastMessage(prepareMsg('gameStarted', [playerNames]));

			console.log("Started game on room: " + this.name);
		}
	}
}

Room.prototype._getInitialState = function(members) {
	var plrs = [];
	_.each(members, function(mem, i) {
		plrs.push(mem.name);
	});

	return {
		m: 'initialState',
		players: plrs
	};
}

Room.prototype._nextTurn = function() {
	// check if everyone has its turn rdy
	var rdy = true;
	_.each(this.members, function(mem, i) {
		rdy = rdy && mem.cmd;
	});

	if (rdy) {

		this.numTurn++;
		_.each(this.members, function(mem, i) {
			this.members[i].cmd = false;
			this.members[i].numTurn ++;
		}, this);

		console.log("Next turn. Turn number: " + this.numTurn);

		var turnData = this.cmds;
		this.cmds = [];
		return turnData;
	} else {
		console.log("Not all players sent their moves.");
		return false;
	}
}

Room.prototype.reportTurn = function(player, command) {
	if (player.numTurn == command.args[0]) {
		player.cmd = true;
		_.each(command.args[1], function (action) {
			this.cmds.push(action);
		}, this);
		console.log("Turn submitted by: " + player.name );
	}

	var thisTurn = this._nextTurn();
	if (thisTurn !== false) {
		this.broadcastMessage(prepareMsg('turn', [thisTurn]));
	}
}

Room.prototype.broadcastMessage = function(msg) {
	_.each(this.members, function(member, i) {
		member.write(msg);
	});
}

Room.prototype.playerLeave = function(player, on_error) {
	if (on_error) {
		this.members.splice(this.members.indexOf(player), 1);
		this.broadcastMessage(prepareMsg('playerLeftRoom', {roomName: this.name, player: player.name}));
	} else {
		this.broadcastMessage(prepareMsg('playerLeftRoom', {roomName: this.name, player: player.name}));
		this.members.splice(this.members.indexOf(player), 1);
	}
}

Room.prototype.reportMatchResult = function(victors, victims) {
	if (!this.hasMatchEnded) {
		_.each(victors, function(victor, i) {
			_.each(victims, function(victim, j) {
				server.reportVictory(victor, victim);
			});
		});
		this.hasMatchEnded = true;
	}
}

Server.prototype.joinRoom = function(player, roomName) {
	var room = this.rooms[roomName];
	if (!room) {
		room = new Room(roomName, player.name)
		this.rooms[roomName] = room;
		console.log("Created room: " + roomName);
	} else if(room.isStarted) {
		console.log('room already started');
		player.write(prepareMsg('roomFull', [room.name]));
		return;
	}
	
	if (room.members.indexOf(player) === -1) {
		if (room.members.length <= room.max) {
			player.roomName = roomName;
			room.members.push(player);
			room.broadcastMessage(prepareMsg('playerJoinedRoom', { roomName: room.name, player: player.name }));
		} else {
			player.write(prepareMsg('roomFull', [room.name]));
		}
	}
}


Server.prototype.removePlayer = function(player, onError) {
	if (player.isOnline) {
		player.isOnline = false;
		this.numPlayersOnline--;
	}
	this.leaveRoom(player, onError);
	console.log("Player disconnected: " + player.name);
}

Server.prototype.leaveRoom = function(player, on_error) {
	var room = this.rooms[player.roomName];
	if (room) {
		room.playerLeave(player, on_error);

		if (room.isStarted) {
			var thisTurn = room._nextTurn();
			if (thisTurn !== false) {
				room.broadcastMessage(prepareMsg('turn', [thisTurn]));
			}

		}

		if (room.members.length == 0) {
			delete this.rooms[player.roomName];
			console.log("Deleted room: " + player.roomName);
		}
		player.roomName = null;
	}
}

Server.prototype._getRooms = function() {
	var ret = [];
	_.each(this.rooms, function(r, i) {
		if (r.isStarted) {
			return;
		}
		ret.push({
			roomName: i,
			roomSize: r.max,
			members: _.map(r.members, function(member) { return member.name; })
		});
	});
	return ret;
}

Server.prototype.getRooms = function(player) {
	var rooms = this._getRooms();
	player.write(prepareMsg('roomsList', [rooms]));
}

Server.prototype.getGlobalStats = function(player) {
	player.write(prepareMsg('globalStats', { 
		numPlayersOnline: this.numPlayersOnline
	}));
}

Server.prototype.getRanking = function(player) {
	var range = 20;
	db.zrank('eloRank', player.name, function(err, rank) {
		if (err == null) {
			function rankingCallback(err, ranking) {
				if (err == null) {
					var result = [ ];
					for (var i = 0; i < ranking.length; i += 2) {
						result.push( { player: ranking[i], score: ranking[i+1] } );
					}
					player.write(prepareMsg('ranking', { 
						rank: rank,
						ranking: result	
					}));
				}
			}

			if (rank == null) {
				db.zrevrange('eloRank', -range, -1, 'withscores', rankingCallback);
			} else {
				db.zrevrange('eloRank', rank - range >= 0 ? rank - range : 0, rank + range, 'withscores', rankingCallback);
			}
		}
	});
}

Server.prototype.logIn = function(player, playerName, password) {
	if (playerName.length > 25 || password.length > 25) {
		player.write(prepareMsg('loginFailed', { error: 'name or password too long' }));
		return;
	}

	if (playerName in this.playersByName && this.playersByName[playerName].isOnline) {
		player.write(prepareMsg('loginFailed', { error: 'already logged in' }));
		return;
	}

	var passwordHash = getSHA1('magicSalt!' + password);

	player.name = playerName;

	this.loadPlayerFromDb(player, function(err) {
		if (err == null) {
			if (player.password == null) {
				player.password = passwordHash;
				player.storePassword();
			} else if (player.password != passwordHash) {
				player.write(prepareMsg('loginFailed', { error: 'wrong password' }));
				return;
			} 
			player.isOnline = true;
			server.numPlayersOnline++;
			server.playersByName[player.name] = player;
			player.write(prepareMsg('loginOk'));
		} else {
			player.write(prepareMsg('loginFailed', { error: 'db error ' + err }));
		}
	});
}

Server.prototype.loadPlayerFromDb = function(player, callback) {
	player.eloRank = -1;
	player.password = null;

	db.hget('passwords', player.name, function(err, password) {
		if (err == null) {
			player.password = password;
			db.zscore('eloRank', player.name, function(err, score) {
				if (err == null) {
					if (score != null) {
						player.eloRank = parseInt(score);
					} else {
						player.eloRank = 1500;
						player.storeEloRank();
					}
					callback(null);
				} else {
					callback(err);
				}
			});
		} else {
			callback(err);
		}
	});

}

Server.prototype.reportTurn = function(player, command) {
	var room = this.rooms[player.roomName];
	if (room) {
		room.reportTurn(player, command);
	}
}

Server.prototype.reportMatchResult = function(player, victors, victims) {
	var room = this.rooms[player.roomName];
	if (room) {
		room.reportMatchResult(victors, victims);
	}
}

Server.prototype.start = function(player) {
	var room = this.rooms[player.roomName];
	if (room && room.ownerName == player.name) {
		room.start();
	}
}

Server.prototype.reportVictory = function(victorName, victimName) {
	var a = this.playersByName[victorName];
	var b = this.playersByName[victimName];
	var s = 400;
	var qa = Math.pow(10, a.eloRank/400);
	var qb = Math.pow(10, b.eloRank/400);
	var ea = qa / (qa + qb);
	var eb = qb / (qa + qb);
	a.eloRank = Math.round(a.eloRank + 16 * (1.0 - ea));
	b.eloRank = Math.round(b.eloRank + 16 * (0.0 - eb));
	a.storeEloRank();
	b.storeEloRank();
}

Server.prototype.handleCommand = function(player, command) {
	console.log("incomming command: ");
	console.log(command);

	switch(command.m) {
		case 'getRooms':
		case 'getGlobalStats':
		case 'getRanking':
		case 'leaveRoom':
		case 'start':
			return this[command.m](player);
		case 'joinRoom':
			return this.joinRoom(player, command.args[0]);
		case 'logIn':
			return this.logIn(player, command.args[0], command.args[1]);
		case 'reportTurn':
			return this.reportTurn(player, command);
		case 'reportMatchResult':
			return this.reportMatchResult(player, command.args[0], command.args[1]);
	}
}

Player.prototype.write = function(msg) {
	this.socket.write(msg + '#');
}

Player.prototype.storeEloRank = function() {
	db.zadd('eloRank', this.eloRank, this.name);
}

Player.prototype.storePassword = function() {
	db.hset('passwords', this.name, this.password);
}

var db = redis.createClient();
db.on('error', function(error) {
	console.log('Redis error: ' + error);
});
db.select(redisDb);

var server = new Server();

net.createServer(function(socket) {
	socket.name = socket.remoteAddress + '+'  + socket.remotePort;
	socket.setEncoding('ascii');

	var player = new Player(socket);
	server.removePlayer(player);
	
	console.log('connected client ' + socket.name + '\n');

	var receivedData = '';
	socket.on('data', function(data) {

		receivedData += data;
		var splitted = receivedData.split('#');
		for (var i = 0;i<splitted.length - 1;i++) {
			console.log(splitted[i]);
			server.handleCommand(player, JSON.parse(splitted[i]));
		}

		receivedData = splitted[splitted.length-1];
	});

	socket.on('end', function() {
		console.log('disc ' + socket.name + '\n');
		server.removePlayer(player);
	});

	socket.on('error', function(error) {
		console.log('err ' + socket.name + '\n');
		console.log(error);
		server.removePlayer(player, true);
	});

	socket.on('close', function() {
		console.log('close ' + socket.name + '\n');
		server.removePlayer(player);
	});

}).listen(listenPort);

console.log('smallcraft server started at port ' + listenPort + '!');

if (crossdomainPort > 0) {
	var crossdomainFile = fs.readFileSync('./crossdomain.xml', 'utf8');
	net.createServer(function(socket) {
		socket.setEncoding('utf8');
		socket.on('data', function(data) {
			if (data == '<policy-file-request/>\0') {
				console.log('crossdomain file requested.');
				socket.end(crossdomainFile, 'utf8');
			} else {
				socket.end();
			}
		});

		socket.on('end', function() {
			socket.end();
		});

		socket.on('error', function(err) {
			if (socket) {
				socket.end();
				socket.destroy();
			}
		});

	}).listen(crossdomainPort);

	console.log('crossdomain server started at port ' + crossdomainPort + '!');
}

