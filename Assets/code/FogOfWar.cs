﻿using UnityEngine;
using System.Collections;

public class FogOfWar {

	public Texture2D texture;
	Color32[] pixels;
	public bool isEnabled;

	public FogOfWar() {
		texture = new Texture2D(Game.mapSize, Game.mapSize, TextureFormat.ARGB32, false);
		pixels = new Color32[Game.mapSize * Game.mapSize];
		isEnabled = true;
	}
	
	public void Tick() {
		if (isEnabled) {
			for (int i = 0; i < Game.mapSize; ++i) {
				for (int j = 0; j < Game.mapSize; ++j) {
					MapTile tile = Game.GetMapTile(i,j);
					if (tile.fowState == FoWState.Visible) {
						tile.fowState = FoWState.Visited;
					}
				}
			}

			foreach (Unit unit in Game.me.units) {
				MapTile unitTile = unit.Tile;
				int radius = (int)unit.model.sightRange;
				int left = Game.ClampToBounds(unitTile.x - radius);
				int right = Game.ClampToBounds(unitTile.x + radius);
				int bottom = Game.ClampToBounds(unitTile.y - radius);
				int top = Game.ClampToBounds(unitTile.y + radius);
				int sqSighRange = (int)(unit.model.sightRange * unit.model.sightRange);
				for (int i = left; i <= right; ++i) {
					for (int j = bottom; j <= top; ++j) {
						if ((i - unitTile.x) * (i - unitTile.x) + (j - unitTile.y) * (j - unitTile.y) < sqSighRange) {
							MapTile tile = Game.GetMapTile(i,j);
							tile.fowState = FoWState.Visible;		
						}
					}
				}
			}
		} else {
			for (int i = 0; i < Game.mapSize; ++i) {
				for (int j = 0; j < Game.mapSize; ++j) {
					MapTile tile = Game.GetMapTile(i,j);
					tile.fowState = FoWState.Visible;
				}
			}
		}

		Color32 visible = new Color(1,1,1,0);
		Color32 visited = new Color(0,0,0,0.5f);
		Color32 hidden = new Color(0,0,0,1);

		for (int i = 0; i < Game.mapSize; ++i) {
			for (int j = 0; j < Game.mapSize; ++j) {
				MapTile tile = Game.GetMapTile(i,j);
				switch (tile.fowState) {
					case FoWState.Visible: pixels[i + j * Game.mapSize] = visible; break;
					case FoWState.Visited: pixels[i + j * Game.mapSize] = visited; break;
					case FoWState.Hidden: pixels[i + j * Game.mapSize] = hidden; break;
				}
			}
		}

		texture.SetPixels32(pixels);
		texture.Apply(false);
	}
}
