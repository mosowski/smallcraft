﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Unit {
	public bool isDead;
	public bool isSelected;
	public bool isGathering;
	public UnitModel model;
	public Vector2 position;
	public Vector2 direction;
	public Vector2 headDirection;
	public Vector2 velocity;

	public List<Feature> features;

	public GameObject gameObject;
	public GameObject carriedObject;

	public Player player;
	public int hp;
	public int energy;

	public Queue<ProductionQueueItem> productionQueue;

	public int lastDamageTick = -99999999;

	public int lastAcknowledgementTick = 0;
	public int lastAcknowledgementId = -1;

	public Order order = null;

	public virtual void Setup(UnitModel model) {
		this.model = model;
		features = new List<Feature>();
		foreach (Feature.Params featureParams in model.features) {
			features.Add(Feature.Create(featureParams, this));
		}
		hp = model.maxHp;
	}

	public T GetFeature<T>() where T:Feature {
		System.Type t = typeof(T);
		for (int i = 0; i < features.Count; ++i) {
			if (features[i].GetType() == t) {
				return features[i] as T;
			}
		}
		return null;
	}

	public virtual void Tick() {
		if (order != null) {
			order.Tick();
		}

		foreach (Feature f in features) {
			f.Tick();
		}

		UpdateProduction();

		if (order == null && GetFeature<AttackFeature>() != null) {
			SetOrder(new DefendPositionOrder(this));
		}
	}

	public virtual MapTile Tile {
		get { return Game.GetMapTile(position); }
	}

	public void CancelOrder() {
		SetOrder(null);
	}

	public void SetOrder(Order order) {
		if (this.order != null) {
			this.order.Cancel();
		}
		this.order = order;
		if (order != null) {
			order.Begin();
		}
	}

	public virtual Vector2 SummonPosition {
		get { 
			MapTile freeTile = Game.GetNearestWalkableTile(position, 5);
			if (freeTile != null) {
				return Game.GetTilePosition(freeTile);
			} else {
				return Vector2.zero;
			}
		}
	}

	public void Damage(int damage) {
		if (model.isIndestructible) {
			return;
		}

		lastDamageTick = Game.currentTickNum;
		hp -= Mathf.Max(0, (damage - model.armor));
		if (hp <= 0) {
			isDead = true;
			if (!string.IsNullOrEmpty(model.destuctionPrefab)) {
				ProjectileManager.CreateProjectile(model.destuctionPrefab, this, null);
			}
		}
	}

	public void QueueProduction(string itemName) {
		if (productionQueue == null) {
			productionQueue = new Queue<ProductionQueueItem>();
		}

		UnitProductionQueueItem item = new UnitProductionQueueItem(this, itemName);
		productionQueue.Enqueue(item);
		item.OnQueued();
	}

	protected void UpdateProduction() {
		if (productionQueue != null && productionQueue.Count > 0) {
			productionQueue.Peek().Tick();
			if (productionQueue.Peek().isCompleted) {
				productionQueue.Dequeue();
			}
		}
	}

	public virtual void Remove() {
		if (productionQueue != null) {
			while (productionQueue.Count > 0) {
				productionQueue.Dequeue().Cancel();
			}
		}

		SetOrder(null);
		IsSelected = false;
		Tile.units.Remove(this);
	}

	public void PlayAcknowledgeSound() {
		if (model.acknowledegeSounds.Count > 0 && Game.currentTickNum - lastAcknowledgementTick > 20) {
			int id = Random.Range(0, model.acknowledegeSounds.Count);
			if (id == lastAcknowledgementId) {
				id = (id + 1) % model.acknowledegeSounds.Count;
			}
			AudioManager.PlaySFX(model.acknowledegeSounds[id], gameObject.transform.position);
			
			lastAcknowledgementId = id;
			lastAcknowledgementTick = Game.currentTickNum;
		}
	}

	public virtual bool IsMoving {
		get {
			return false;
		}
	}

	public virtual float HeightOffset {
		get { return 0; }
	}


	public bool IsSelected {
		get {
			return isSelected;
		}
		set {
			isSelected = value;
		}
	}

	public bool IsProducing {
		get {
			return productionQueue != null && productionQueue.Count > 0;
		}
	}

	public bool IsGathering {
		get {
			return isGathering;
		}
	}

	public virtual bool IsComplete {
		get {
			return true;
		}
	}

	public int CurrentProductionTotalTime {
		get {
			return productionQueue.Peek().ProductionTime;
		}
	}

	public int CurrentProductionLeftTime {
		get {
			return productionQueue.Peek().numTicksLeft;
		}
	}
}
