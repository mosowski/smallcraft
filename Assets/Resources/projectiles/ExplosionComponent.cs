﻿using UnityEngine;
using System.Collections;

public enum ExplosionLocation {
	Spawn,
	Target
}

public class ExplosionComponent : ProjectileComponent {

	public float timeToLive = 1.0f;
	public string sfxId = "";
	public Vector3 offset = Vector3.zero;
	public ExplosionLocation location = ExplosionLocation.Spawn;
	float currentTimeToLive;

	public override void Setup(Unit attacker, Unit target) {
		base.Setup(attacker, target);
		if (location == ExplosionLocation.Spawn) {
			transform.position = attacker.gameObject.transform.position + offset;
		} else {
			transform.position = target.gameObject.transform.position + offset;
		}
		GetComponent<ParticleSystem>().Clear();
		GetComponent<ParticleSystem>().time = 0;
		currentTimeToLive = 1.0f;

		if (sfxId != "") {
			AudioManager.PlaySFX(sfxId, Vector3.zero);
		}
	}

	public override void Setup(Vector3 from, Vector3 to) {
		base.Setup(from, to);
		if (location == ExplosionLocation.Spawn) {
			transform.position = from + offset;
		} else {
			transform.position = to + offset;
		}
		GetComponent<ParticleSystem>().Clear();
		GetComponent<ParticleSystem>().time = 0;
		currentTimeToLive = 1.0f;

		if (sfxId != "") {
			AudioManager.PlaySFX(sfxId, Vector3.zero);
		}
	}

	void Update() {
		currentTimeToLive -= Time.deltaTime;
		if (currentTimeToLive <= 0) {
			ProjectileManager.ReleaseProjectile(gameObject);
		}
	}
}
