﻿using UnityEngine;
using System.Collections;

public class RocketComponent : ProjectileComponent {

	float timeToLive;

	public override void Setup(Unit attacker, Unit target) {
		base.Setup(attacker, target);
		timeToLive = 0.5f;
	}

	void Update() {
		timeToLive -= Time.deltaTime;
		float progress = (1.0f - timeToLive / 0.5f);
		progress *= progress;
		Vector3 direction = targetStartPosition - attackerStartPosition;
		transform.position = attackerStartPosition + direction * progress;
		transform.rotation = Quaternion.Euler(0, Mathf.Atan2(direction.z, -direction.x) * Mathf.Rad2Deg, 0);
		if (timeToLive <= 0) {
			ProjectileManager.ReleaseProjectile(gameObject);
			ProjectileManager.CreateProjectile("SmallExplosion", transform.position, Vector3.zero);
		}
	}
}
