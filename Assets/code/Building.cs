﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Building : Unit {

	public int completness; 
	public int constructionHp;

	public int anchorCol;
	public int anchorRow;
	public Vector2 rallyOffset;

	public override void Setup(UnitModel model) {
		base.Setup(model);
		completness = 0;
		constructionHp = 1;
		hp = 1;
	}

	public void PlaceOnMap() {
		for (int i = anchorCol; i < anchorCol + model.tileWidth; ++i) {
			for (int j = anchorRow; j < anchorRow + model.tileHeight; ++j) {
				Game.GetMapTile(i, j).extraCost += 1000;
				Game.GetMapTile(i, j).units.Add(this);
			}
		}
	}

	public void RemoveFromMap() {
		for (int i = anchorCol; i < anchorCol + model.tileWidth; ++i) {
			for (int j = anchorRow; j < anchorRow + model.tileHeight; ++j) {
				Game.GetMapTile(i, j).extraCost -= 1000;
				Game.GetMapTile(i, j).units.Remove(this);
			}
		}
	}

	public override void Tick() {
		base.Tick();

		if (!IsComplete) {
			Construct();
		}
	}

	public virtual void Construct() {
		if (completness != model.constructionTime) {
			completness++;
			int hpIncrease = completness * model.maxHp / model.constructionTime - constructionHp;
			hp += hpIncrease;
			constructionHp += hpIncrease;
		}
	}

	public virtual void CompleteNow() {
		completness = model.constructionTime;
		hp = model.maxHp;
	}

	public override bool IsComplete {
		get {
			return completness == model.constructionTime;
		}
	}

	public override MapTile Tile {
		get { return Game.GetMapTile(anchorCol, anchorRow); }
	}

	public override void Remove() {
		RemoveFromMap();
		base.Remove();
	}
}
