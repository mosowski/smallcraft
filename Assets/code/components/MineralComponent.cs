﻿using UnityEngine;
using System.Collections;

public class MineralComponent : BaseUnitComponent {

	public enum BodyState {
		Poor,
		Rich,
		None
	}

	protected BodyState bodyState;
	protected Mineral mineral;
	protected Quaternion rotation;

	public override void Setup(Unit unit) {
		base.Setup(unit);
		mineral = unit as Mineral;
		bodyState = BodyState.None;

		SetPositionImmediately();
		rotation = Quaternion.Euler(0, Random.Range(0,4) * 90, 0);
	}

	protected virtual void SetRichState() {
		DestroyBody();
		body = GameObjectPool.Create("units/body/MineralRich");
		AttachBody();
		bodyState = BodyState.Rich;
	}

	protected virtual void SetPoorState() {
		DestroyBody();
		body = GameObjectPool.Create("units/body/MineralPoor");
		AttachBody();
		bodyState = BodyState.Poor;
	}

	protected virtual void UpdateMineral() {
		if (!mineral.isDead) {
			if (mineral.minerals > 750 && bodyState != BodyState.Rich) {
				SetRichState();
			} else if (mineral.minerals <= 750 && bodyState != BodyState.Poor) {
				SetPoorState();
			}
		}
	}

	void Update () {
		UpdateMineral();
		UpdateGameObject();
		transform.rotation = rotation;
	}
}
