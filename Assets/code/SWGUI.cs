using UnityEngine;
using System.Collections.Generic;
using LitJson;

public class SWGUI
{
	static string pName = "";
	static string rName = "";
	static string password = "";
	static string errorMessage = "";
	static Vector3 clickPosition;
	static Vector2 manyUnitsScrollPosition = Vector2.zero;
	static Vector2 rankingScrollPosition = Vector2.zero;

	static int did = 0;
	static string menuState = "login";

	public static void OnTick(Root root, Client client) {
		CommonAssets.updateSizes();

		if (!Game.isPlaying) {
			//DrawMainMenu(root, client);
		} else {
			Player myPlayer = Game.me;

			float scaleFactor = Camera.main.pixelWidth / CommonAssets.UIBG.width;

			// draw HUD
			GUI.DrawTexture (new Rect(0f, Camera.main.pixelHeight - CommonAssets.UIBG.height * scaleFactor, Screen.width, CommonAssets.UIBG.height * scaleFactor), CommonAssets.UIBG);
			DrawTopHUD(myPlayer.gold, myPlayer.usedSupply, myPlayer.providedSupply);

			// draw Minimap (on TOP)
			DrawMinimap (root, scaleFactor);


			if (myPlayer.selection.Count > 1) {
				// group view
				//GUI.Label(new Rect(600 * scaleFactor, Camera.main.pixelHeight + (235 - CommonAssets.UIBG.height) * scaleFactor, 600 * scaleFactor, 40 * scaleFactor), "Selected: " + myPlayer.selection.Count + " units");
				DrawManyUnitsHUD(root, myPlayer.selection, scaleFactor);

				DrawMobileActions(root, myPlayer.selection, scaleFactor);
			} else if (myPlayer.selection.Count == 1) {
				if (ClientMagicBox.Get("buildMode")) {
					DrawBuildingsOptions(root, myPlayer.selection[0], scaleFactor);
				} else {
					DrawMobileUnitInfo (root, myPlayer.selection[0], scaleFactor);
				}
				DrawMobileActions(root, myPlayer.selection, scaleFactor);

			}

			if (Event.current.Equals (Event.KeyboardEvent ("return"))) {
				ClientMagicBox.ToggleChatBox();
				Event.current.Use();
			} else if (ClientMagicBox.Get("ChatBox")) {
        		GUIStyle chatbox = new GUIStyle(CommonAssets.GUITextfieldStyle);
        		chatbox.alignment = TextAnchor.MiddleLeft;
        		RectOffset padding = new RectOffset(10, 10, 0, 0);
        		chatbox.padding = padding;
				GUILayout.BeginArea(new Rect(0, 700 * scaleFactor, 1000*scaleFactor, 100*scaleFactor));
				GUI.SetNextControlName ("ChatBox");
				ClientMagicBox.chatText = GUILayout.TextField(ClientMagicBox.chatText, chatbox);
				GUI.FocusControl ("ChatBox");
				GUILayout.EndArea();
			}

			Notifications.Draw(scaleFactor);
		}
	}

	private static void DrawBuildingsOptions(Root root, Unit selected, float scaleFactor) 
	{
		string[] producingList;

		producingList = selected.GetFeature<ProductionFeature>().p.producedUnits;

		GUIStyle iconStyle = new GUIStyle();
		iconStyle.fixedWidth = 70 * scaleFactor;
		iconStyle.fixedHeight = 70 * scaleFactor;

		GUIStyle sub = new GUIStyle();
		sub.alignment = TextAnchor.MiddleCenter;
		sub.fontSize = (int)(14 * scaleFactor);
		sub.normal.textColor = Color.white;

		GUILayout.BeginArea(new Rect(605 * scaleFactor,getGameScreenHeight(CommonAssets.UIBG_minHeight - 27), 700 * scaleFactor, 178 * scaleFactor));
		manyUnitsScrollPosition = GUILayout.BeginScrollView(manyUnitsScrollPosition, false, false, new GUILayoutOption[] { GUILayout.Height(176*scaleFactor), GUILayout.Width(690 * scaleFactor) });

		int j = 0;
		foreach(string u in producingList) {
			GUILayout.BeginArea(new Rect(j*70 * scaleFactor,  0, 70 * scaleFactor, 100 * scaleFactor));
			GUIContent x = new GUIContent();
			UnitModel uModel = Game.me.model.GetModel(u);
			x.image = IconsManager.GetSmallIcon(u);
			if (GUILayout.Button(x, iconStyle)) {
				if (uModel.tileWidth == 0) {
					Actions.QueueActionQueueProductionOrder(uModel.id);
				} else {
					if (Game.me.CheckDependencies(uModel.id)) {
						if (Game.me.gold >= uModel.cost) {
							Camera.main.GetComponent<CameraMover>().EnterConstructionMode(u);
						} else {
							Notifications.AddLocal(Game.me, "To build " + uModel.displayName + " you need " + uModel.cost + " minerals");
						}
					} else {
						Notifications.AddLocal(Game.me, "To build " + uModel.displayName + " you need " + Game.me.model.GetProductionDependenciesListed(uModel));
					}
				}
			}
			GUILayout.Label(uModel.cost.ToString(), sub);
			GUILayout.EndArea();
			j++;
		}

		if (selected.productionQueue != null) {
			j = 0;
			foreach(ProductionQueueItem b in selected.productionQueue) {
				GUILayout.BeginArea(new Rect(j*70 * scaleFactor,  100 * scaleFactor, 70 * scaleFactor, 78 * scaleFactor));
				GUIContent x = new GUIContent();
				x.image = IconsManager.GetSmallIcon(b.itemName);
				GUILayout.Button(x, iconStyle);
				GUILayout.EndArea();
				j++;
			}
		}

		GUILayout.EndScrollView();
		GUILayout.EndArea();
	}


	private static void DrawMobileActions(Root root, List<Unit> units, float scaleFactor)
	{
		GUIStyle style = new GUIStyle();
		style.fixedWidth = 90 * scaleFactor;
		style.fixedHeight = 90 * scaleFactor;
		GUILayout.BeginArea(new Rect(1420 * scaleFactor, getGameScreenHeight(CommonAssets.UIBG_topHeight - 60), 440 * scaleFactor, 230 * scaleFactor));
		GUILayout.BeginVertical();
		GUILayout.BeginHorizontal();

		if (ClientMagicBox.CheckSelectionFor<MoveFeature>()) {
			if(GUILayout.Button (CommonAssets.UI_ActionMoveInactive, ClientMagicBox.Get("moveMode") ? CommonAssets.OrderMoveActive : CommonAssets.OrderMoveInactive)) {
				ClientMagicBox.MoveMode();
			}
		}

		if (ClientMagicBox.CheckSelectionFor<AttackFeature>()) {
			if(GUILayout.Button (CommonAssets.UI_ActionAttackInactive, ClientMagicBox.Get("attackMode") ? CommonAssets.OrderAttackActive : CommonAssets.OrderAttackInactive)) {
				ClientMagicBox.AttackMode();
			}
	    }

		if (ClientMagicBox.CheckSelectionFor<AttackFeature>() && ClientMagicBox.CheckSelectionFor<MoveFeature>()) {
			if(GUILayout.Button (CommonAssets.UI_ActionSpecialInactive, ClientMagicBox.Get("attackMoveMode") ? CommonAssets.OrderAttackMoveActive : CommonAssets.OrderAttackMoveInactive)) {
				ClientMagicBox.AttackMoveMode();
			}
	    }
/*
		if (ClientMagicBox.CheckSelectionFor("stop")) {
			if(GUILayout.Button (
				CommonAssets.UI_ActionStopInactive, CommonAssets.OrderAttackInactive
			)) {
				ClientMagicBox.Stop();
			}
		}
		*/

		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();

		if (ClientMagicBox.CheckSelectionFor<GatherFeature>()) {
			if(GUILayout.Button (CommonAssets.UI_ActionGatherInactive, ClientMagicBox.Get("gatherMode") ? CommonAssets.OrderGatherActive : CommonAssets.OrderGatherInactive)) {
				ClientMagicBox.GatherMode();
			}
		}

		if (ClientMagicBox.CheckSelectionFor<ProductionFeature>()) {
			if(GUILayout.Button (CommonAssets.UI_ActionBuildInactive, ClientMagicBox.Get("buildMode") ? CommonAssets.OrderBuildActive : CommonAssets.OrderBuildInactive)) {
				ClientMagicBox.BuildMode();
			}
		}

		GUILayout.EndHorizontal();
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}

	private static void DrawMobileUnitInfo(Root root, Unit unit, float scaleFactor) {

		UnitModel m = unit.model;

		GUILayout.BeginArea(new Rect(585 * scaleFactor,getGameScreenHeight(CommonAssets.UIBG_minHeight - 30),726 * scaleFactor,167 * scaleFactor));
		GUI.DrawTexture (new Rect(0,0, CommonAssets.UI_MobStats.width * scaleFactor, CommonAssets.UI_MobStats.height * scaleFactor), CommonAssets.UI_MobStats);

		GUI.DrawTexture (new Rect(35 * scaleFactor, 55 * scaleFactor, 90 * scaleFactor, 90 * scaleFactor), IconsManager.GetBigIcon(m.id));

		GUIStyle label_style = new GUIStyle();
		label_style.font = CommonAssets.label_font;
		label_style.fontSize = (int)(28 * scaleFactor);
		label_style.alignment =  TextAnchor.MiddleCenter;
		label_style.onNormal.textColor = new Color(38,42,53);
		label_style.onActive = label_style.onNormal;

		// unit name
		GUI.Label (new Rect(19 * scaleFactor, 19 * scaleFactor, 280 * scaleFactor, 28 * scaleFactor), m.displayName.ToUpper(), label_style);

		// unit type
		string unitType = unit.GetType().Name;
		label_style.fontSize = (int)(15 * scaleFactor);
		GUI.Label (new Rect(175 * scaleFactor, 54 * scaleFactor, 125 * scaleFactor, 15 * scaleFactor), unitType.ToUpper(), label_style);		// mocked type!!

		// unit HP & energy
		label_style.onNormal.textColor = Color.white;
		label_style.fontSize = (int)(22 * scaleFactor);
		GUI.Label (new Rect(175 * scaleFactor, 89 * scaleFactor, 125 * scaleFactor, 30 * scaleFactor), unit.hp + "/" + m.maxHp, label_style);
		GUI.Label (new Rect(175 * scaleFactor, 130 * scaleFactor, 125 * scaleFactor, 30 * scaleFactor), unit.energy + "/" + m.maxEnergy, label_style);

		//GUILayout.BeginVertical(new Rect(0, 167 * scaleFactor * 0.5, 726 * scaleFactor,167 * scaleFactor * 0.5));
		//GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	private static void DrawMainMenu(Root root, Client client)
	{
		GUI.DrawTexture(new Rect(0,0,Camera.main.pixelWidth, Camera.main.pixelHeight), CommonAssets.MenuBG);

		GUIStyle areaStyle = new GUIStyle();
		areaStyle.alignment = TextAnchor.MiddleCenter;

		GUILayout.BeginArea(new Rect(0,0,Camera.main.pixelWidth, Camera.main.pixelHeight), areaStyle);
		GUILayout.BeginHorizontal(areaStyle);
		GUILayout.FlexibleSpace();
		GUILayout.BeginVertical(areaStyle);

		if (menuState == "login") {
			GUILayout.Box (CommonAssets.Logo, CommonAssets.LogoStyle);

			GUILayout.BeginVertical();

			GUILayout.Label("Login:");
			pName = GUILayout.TextField(pName, 25, CommonAssets.GUITextfieldStyle);
			GUILayout.Label("Password (optional):");
			password = GUILayout.PasswordField(password, '*', 25, CommonAssets.GUITextfieldStyle);
			GUILayout.Label(" "); // the instrument of doom

			if (GUILayout.Button("PLAY", CommonAssets.GUIButtonWideStyle) && pName != "") {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				Lobby.LogIn(pName, password);
				Lobby.GetRooms();
			}

			if (errorMessage != "") {
				GUILayout.Label(errorMessage);
			}

			GUILayout.EndVertical();
		} else if (menuState == "lobby") {
			foreach (RoomInfo room in Lobby.rooms) {

				if (DrawRoom(room.name, room.players, room.maxPlayers)) {
					AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
					Lobby.JoinRoom(room.name);
				}
			}
			
			GUILayout.BeginHorizontal();

			if (GUILayout.Button ("REFRESH ROOMS", CommonAssets.GUIButtonWideStyle)) {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				Lobby.GetRooms();
			}

			if (GUILayout.Button ("RANKING", CommonAssets.GUIButtonWideStyle)) {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				Lobby.GetRanking();
			}

			GUILayout.EndHorizontal();

			GUILayout.Label("Players online: " + Lobby.numPlayersOnline);


			GUIStyle littleMargin = new GUIStyle();
			littleMargin.margin = new RectOffset(0,0,40,0);
			GUILayout.BeginHorizontal(littleMargin);

			rName = GUILayout.TextField (rName, CommonAssets.GUITextfieldStyle);
			if (GUILayout.Button ("CREATE ROOM", CommonAssets.GUIButtonWideStyle) && rName != "") {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				Lobby.JoinRoom(rName);
			}

			GUILayout.EndHorizontal();
		} else if (menuState == "room") {

			RoomInfo room = Lobby.CurrentRoom;
			if (room != null) {
				foreach(string member in room.members) {
					GUILayout.Label (member, CommonAssets.GUIPlayerStyle);
				}
			}

			if (GUILayout.Button ("REFRESH ROOM", CommonAssets.GUIButtonWideStyle)) {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				Lobby.GetRooms();
			}

			GUILayout.BeginHorizontal();

			if (GUILayout.Button ("EXIT ROOM", CommonAssets.GUIButtonWideStyle)) {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				Lobby.LeaveRoom();
			}
			
			if (GUILayout.Button("START GAME", CommonAssets.GUIButtonWideStyle)) {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				client.Call("start");
			}

			GUILayout.EndHorizontal();
		} else if (menuState == "ranking") {
			rankingScrollPosition = GUILayout.BeginScrollView(rankingScrollPosition, GUILayout.Width(700), GUILayout.Height(500));
			foreach(RankingItem ri in Lobby.ranking) {
				GUILayout.Label(ri.name + ":  " + ri.score, CommonAssets.GUIPlayerStyle);
			}
			GUILayout.EndScrollView();
			if (GUILayout.Button ("RETURN", CommonAssets.GUIButtonWideStyle)) {
				AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
				menuState = "lobby";
			}
		}

		GUILayout.FlexibleSpace();
		GUILayout.EndVertical();
		GUILayout.FlexibleSpace();
		GUILayout.EndHorizontal();
		GUILayout.EndArea();
	}

	private static bool DrawRoom(string name, int players, int maxplayers)
	{
		GUILayout.BeginHorizontal(CommonAssets.RoomStyle);

		GUILayout.Label(name, CommonAssets.GUIFrameYellow);
		GUILayout.Label(players + "/" + maxplayers, CommonAssets.RoomSizeStyle);
		bool clicked = (players < maxplayers) ? GUILayout.Button ("JOIN", CommonAssets.GUIJoinButtonStyle) : false;

		GUILayout.EndHorizontal();

		return clicked;
	}

	private static void DrawManyUnitsHUD(Root root, List<Unit> units, float scaleFactor) 
	{
		GUILayoutOption[] horizontalOptions = new GUILayoutOption[] {
			GUILayout.MaxWidth(720 * scaleFactor)
		};
		GUIStyle iconStyle = new GUIStyle();
		iconStyle.padding = new RectOffset(0,0,0,0);
		iconStyle.margin = new RectOffset(1,1,1,1);
		iconStyle.border = new RectOffset(2,2,2,2);

		GUILayout.BeginArea(new Rect(605 * scaleFactor,getGameScreenHeight(CommonAssets.UIBG_minHeight - 27), 700 * scaleFactor, 178 * scaleFactor));
		manyUnitsScrollPosition = GUILayout.BeginScrollView(manyUnitsScrollPosition, false, false, new GUILayoutOption[] { GUILayout.Height(176*scaleFactor), GUILayout.Width(690 * scaleFactor) });

		int j = 0;
		GUILayout.BeginHorizontal(horizontalOptions);
		foreach(Unit u in units) {
			GUIContent x = new GUIContent();
			x.image = IconsManager.GetSmallIcon(u.model.id);

			if (GUILayout.Button(x, CommonAssets.GUIIconStyle)) {
				Debug.Log ("Clicked unit ");
			}
			if (++j % 9 == 0)  { 
				GUILayout.EndHorizontal();
				GUILayout.BeginHorizontal(horizontalOptions);
			}
		}

		GUILayout.EndHorizontal();
		GUILayout.EndScrollView();
		GUILayout.EndArea();
	}

	private void clearManyUnitsScroll() {
		manyUnitsScrollPosition = Vector2.zero;
	}

	private static void DrawTopHUD(int res1, int res2, int maxRes2)
	{
		GUIStyle left_align = new GUIStyle();
		left_align.alignment = TextAnchor.MiddleRight;
		left_align.fontSize = 21;
		left_align.normal.textColor = Color.white;

		GUI.DrawTexture(new Rect(0,0,CommonAssets.UITop.width,CommonAssets.UITop.height), CommonAssets.UITop);
		GUI.Label (new Rect(50,7,80,28), res1.ToString(), left_align);
		GUI.Label (new Rect(190,7,80,28), res2.ToString() + "/" + maxRes2.ToString(), left_align);
	}

	public static float getGameScreenHeight(float HUDHeight) {
		float scaleFactor = Camera.main.pixelWidth / CommonAssets.UIBG.width;
		return Camera.main.pixelHeight - HUDHeight * scaleFactor;
	}

	// x,y - counted from top-left corner
	public static bool isOutsideHUD(float x, float y)
	{
		if (ClientMagicBox.Get("ChatBox")) {
			return false;
		}
		if (x < CommonAssets.UIBG_minLeft || x > CommonAssets.UIBG_minRight) {
			return y < getGameScreenHeight(CommonAssets.UIBG_topHeight);
		} else {
			return y < getGameScreenHeight(CommonAssets.UIBG_minHeight);
		}
	}


	private static Color[] minimapPixels;
	public static void DrawMinimap(Root root, float factor)
	{
		int minimapSize = (int)(236 * factor);

		if (CommonAssets.minimap == null) {
			CommonAssets.minimap = new Texture2D(Game.mapSize, Game.mapSize);
		}

		if (minimapPixels == null) {
			minimapPixels = new Color[Game.mapSize * Game.mapSize];
			for(int i = 0; i < Game.mapSize; i++) {
				for (int j = 0; j < Game.mapSize; j++) {
					MapTile mt = Game.map[j+1, i+1];

					switch (mt.terrainType) {
					case TerrainType.High:
						minimapPixels[i * Game.mapSize + j] = new Color(153f/255f, 102f/255f, 51f/255f, 0.4f);
						break;
					case TerrainType.Low:
						minimapPixels[i * Game.mapSize+ j] = new Color(204f/255f, 1f, 153f/255f, 0.4f);
						break;
					case TerrainType.Water:
						minimapPixels[i * Game.mapSize + j] = new Color(0.6f, 0.6f, 1f, 0.4f);
						break;
					}						
				}
			}
		}

		CommonAssets.minimap.SetPixels(0,0,Game.mapSize, Game.mapSize, minimapPixels);

		foreach(Unit unit in Game.units) {
			if (unit.Tile.fowState == FoWState.Visible && unit.player != Game.neutral) {
				float radius = unit.model.clickRadius;

				for(int x = (int)(unit.position[0] - radius); x <= (int)(unit.position[0] + radius); x++) {
					for(int y = (int)(unit.position[1] - radius); y <= (int)(unit.position[1] + radius); y++) {
						CommonAssets.minimap.SetPixel(x, y, MaterialManager.playerColors[unit.player.number]);
					}
				}
			}
		}

		DrawMiniMapLine(CameraMover.screenTL, CameraMover.screenTR);
		DrawMiniMapLine(CameraMover.screenTR, CameraMover.screenBR);
		DrawMiniMapLine(CameraMover.screenBR, CameraMover.screenBL);
		DrawMiniMapLine(CameraMover.screenBL, CameraMover.screenTL);

		CommonAssets.minimap.Apply();

		float mmLeft = 56 * factor;
		float mmTop = getGameScreenHeight(CommonAssets.UIBG_topHeight - 67);
		GUI.DrawTexture(new Rect(mmLeft, mmTop, minimapSize, minimapSize), CommonAssets.minimap);
		GUI.DrawTexture(new Rect(mmLeft, mmTop, minimapSize, minimapSize), Game.fow.texture);

		if (Input.mousePosition.x > mmLeft && Camera.main.pixelHeight - Input.mousePosition.y > mmTop &&
			Input.mousePosition.x < mmLeft + minimapSize && Camera.main.pixelHeight - Input.mousePosition.y < mmTop + minimapSize) {
			Vector2 gamePosition = new Vector2(
				((Input.mousePosition.x - mmLeft) / minimapSize) * Game.mapSize,
				(1.0f - (Camera.main.pixelHeight - Input.mousePosition.y - mmTop) / minimapSize) * Game.mapSize
			);
			if (Event.current.type == EventType.MouseDown && Event.current.button == 0) {
				ClientMagicBox.OnLMBClickFromMiniMap(gamePosition);
				clickPosition = Input.mousePosition;
				Event.current.Use();
			} else if (Input.GetMouseButton(0) && clickPosition != Vector3.zero && clickPosition != Input.mousePosition) {
				ClientMagicBox.OnLMBDragOnMiniMap(gamePosition);
			}

			if (Input.GetMouseButtonUp(0)) {
				clickPosition = Vector3.zero;
			}

			if (Event.current.type == EventType.MouseDown && Event.current.button == 1) {
				ClientMagicBox.OnRMBClick(gamePosition);
				Event.current.Use();
			}

		}
	}

	static void DrawMiniMapLine(Vector2 a, Vector2 b) {
		DrawMiniMapLine((int)a.x, (int)a.y, (int)b.x, (int)b.y);
	}

	static void DrawMiniMapLine(int x1, int y1, int x2, int y2) {
		foreach (Point p in Bresenham.Line(x1, y1, x2, y2)) {
			if (p.x >= 0 && p.y >= 0 && p.x < CommonAssets.minimap.width && p.y < CommonAssets.minimap.height) {
				CommonAssets.minimap.SetPixel(p.x, p.y, Color.white);
			}
		}
	}
}
