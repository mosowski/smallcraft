  Shader "Decal Bumped Colored" {
    Properties {
		_Color ("Main Color", Color) = (1,1,1,0.5)
		_DecalColor ("Decal Color", Color) = (1,1,1,0.5)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
		_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
		_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
		_BumpMap ("Normalmap", 2D) = "bump" {}
		_DecalTex ("Decal", 2D) = "white" { }
    }
    SubShader {
      Tags { "RenderType" = "Opaque" }
      CGPROGRAM
      #pragma surface surf BlinnPhong 

      struct Input {
          float2 uv_MainTex;
		  float3 viewDir;
      };
      sampler2D _MainTex;
      sampler2D _BumpMap;
      sampler2D _DecalTex;
	  float4 _Color;
	  float4 _DecalColor;
	  float _Shininess;
      void surf (Input IN, inout SurfaceOutput o) {
		half4 texcol = tex2D (_MainTex, IN.uv_MainTex);
		half4 deccol = tex2D (_DecalTex, IN.uv_MainTex) * _DecalColor;
		deccol.xyz *= deccol.w;
		o.Albedo = (1.0f - deccol.w) * texcol * _Color + deccol;
		o.Gloss = texcol.a;
		o.Specular = _Shininess;
		o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }
