﻿using UnityEngine;
using System.Collections;

public class MapPreserver : MonoBehaviour {
	void Start () {
		DontDestroyOnLoad(gameObject);
		DontDestroyOnLoad(GameObject.Find("StartingLocation1"));
		DontDestroyOnLoad(GameObject.Find("StartingLocation2"));
		DontDestroyOnLoad(GameObject.Find("StartingLocation3"));
		DontDestroyOnLoad(GameObject.Find("StartingLocation4"));
		Application.LoadLevel("main");
	}
}
