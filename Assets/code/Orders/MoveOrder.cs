﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class MoveOrder : Order {

	protected List<AStar.Node> cachedPath;
	protected MoveFeature moveFeature;

	public MoveOrder(Unit unit): base(unit) {
		moveFeature = unit.GetFeature<MoveFeature>();
	}

	public MoveOrder(Unit unit, Vector2 targetPoint) : this(unit) {
		SetTargetPoint(targetPoint);
	}

	protected void SetTargetPoint(Vector2 targetPoint) {
		moveFeature.targetTile = Game.GetMapTile(targetPoint);
		moveFeature.targetPoint = targetPoint;
		moveFeature.leaders = new List<Unit>();
		moveFeature.reachRadius = unit.model.smallRadius;
	}

	protected virtual MapTile GetTargetTile() {
		return moveFeature.targetTile;
	}

	public override void Tick() {
		MobileUnit u = unit as MobileUnit;
		Vector2 finalDest = moveFeature.targetPoint;
		if (unit.model.isFlying) {
			moveFeature.boidFollow = (finalDest - u.position).normalized * u.model.maxSpeed * 8;
		} else {
			MapTile unitMapTile = u.Tile;
			bool isWalled = !Game.IsWalkable(unitMapTile);

			MobileUnit leader = GetValidLeader();
			if (leader != null) {
				moveFeature.leaders.Remove(unit);
				moveFeature.boidFollow = leader.position - u.position;
				moveFeature.boidAlign = leader.velocity;
			} else {
				if (!moveFeature.leaders.Contains(unit)) {
					moveFeature.leaders.Add(unit);
					InitCachedPath();
				}

				//XXX
				if (cachedPath.Count == 0) {
					InitCachedPath();
				}

				if (cachedPath[0].x == unitMapTile.x && cachedPath[0].y == unitMapTile.y) {
					cachedPath.RemoveAt(0);
				}

				if (cachedPath.Count == 0) {
					OnRechedDestination();
					return; 
				}
				Vector2 nextOnPath = new Vector2(cachedPath[0].x - 0.5f, cachedPath[0].y - 0.5f);

				moveFeature.boidFollow = nextOnPath - u.position;

				if (Game.IsStraightPathWalkable(u.position, nextOnPath) || isWalled) {
					moveFeature.boidFollow = nextOnPath - u.position;
					Game.DebugLine(u.position, nextOnPath, Color.white, 1.0f);
				} else {
					MapTile end = Game.GetMapTile(nextOnPath);
					int stepsLimit = (Mathf.Abs(end.x - unitMapTile.x) + Mathf.Abs(end.y - unitMapTile.y)) * 3;
					List<AStar.Node> res = AStar.GridPath(Game.map, unitMapTile.x, unitMapTile.y, end.x, end.y, stepsLimit).ToList();
					if (res.Count > 0) {
						AStar.Node tmp = res[0];
						Vector2 target = new Vector2(tmp.x - 0.5f, tmp.y - 0.5f);
						if (unitMapTile.x == tmp.x && unitMapTile.y == tmp.y && res.Count > 1) {
							target = new Vector2(res[1].x - 0.5f, res[1].y - 0.5f);
						}
						moveFeature.boidFollow = target - u.position;
						//Game.DebugLine(u.position, target, Color.blue, 1.0f);
						DrawAStarPath(res, Color.blue, 2.0f);
					} else {
						moveFeature.boidFollow = new Vector2(end.x - unitMapTile.x, end.y - unitMapTile.y).normalized * u.model.maxSpeed;
					}
				}

				moveFeature.boidFollow = moveFeature.boidFollow * 20;
			}
		}

		if (Mathf.Abs(Vector2.Distance(u.position, finalDest)) < moveFeature.reachRadius) { // global check when to cancel order
   			OnRechedDestination();
   		}

	}

	protected virtual void OnRechedDestination() {
		unit.CancelOrder();
	}

	private void DrawAStarPath(List<AStar.Node> path, Color c, float d = 10.0f) {
		AStar.Node prev = null;
		foreach (AStar.Node node in path) {
			if (prev != null) {
				Game.DebugLine(new Vector2(prev.x, prev.y), new Vector2 (node.x, node.y), c, d);
			}
			prev = node;
		}
	}

	protected void InitCachedPath() {
		MobileUnit u = unit as MobileUnit;
		MapTile unitMapTile = Game.GetMapTile(u.position);
		IEnumerable<AStar.Node> path = AStar.GridPath(Game.map, unitMapTile.x, unitMapTile.y, GetTargetTile().x, GetTargetTile().y);
		List<AStar.Node> tmp = path.ToList();
		cachedPath = new List<AStar.Node>();
		int j = 0;
		for(int i = tmp.Count - 1;i>=0;i--) {
			if (j % 5 == 0) {
				cachedPath.Add(tmp[i]);
			}
			j++;
		}
		cachedPath.Reverse();
		DrawAStarPath(cachedPath, Color.black, 5.0f);
	}

	private MobileUnit GetValidLeader() {
		if (moveFeature.leaders.Contains(unit)) {
			return null;
		}
		foreach(MobileUnit leader in moveFeature.leaders) {
			if (leader.model.isFlying == unit.model.isFlying && 
					(leader.model.isFlying || Game.IsStraightPathWalkable(unit.position, leader.position)) &&
					 unit.model.maxSpeed == leader.model.maxSpeed) {
				return leader as MobileUnit;
			}
		}
		return null;
	}

	public static void SetToGroup<T>(IEnumerable<Unit> units, Vector2 targetPoint) where T : MoveOrder {
		List<Unit> leaders = new List<Unit>();
		
		MapTile targetTile = Game.GetMapTile(targetPoint);
		float groupArea = 0;
		foreach (Unit unit in units) {
			groupArea += unit.model.smallRadius *  unit.model.smallRadius * Mathf.PI;
		}
		groupArea *= 1.3f;
		float reachRadius = Mathf.Sqrt(groupArea / Mathf.PI);

		foreach (Unit unit in units) {
			MoveFeature moveFeature = unit.GetFeature<MoveFeature>();
			if (moveFeature != null) {
				moveFeature.targetTile = targetTile;
				moveFeature.targetPoint = targetPoint;
				moveFeature.leaders = leaders;
				moveFeature.reachRadius = reachRadius;

				unit.SetOrder((T)Activator.CreateInstance(typeof(T), unit));
			}
		}
	}
}

