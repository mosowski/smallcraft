﻿using UnityEngine;
using System.Collections;

public class FogOfWarComponent : MonoBehaviour {

	void Start() {
	}

	void Update() {
		if (Game.fow != null) {
			renderer.material.mainTexture = Game.fow.texture;
		}
	}
}
