﻿using UnityEngine;
using System.Collections;

public class Cheats {
	public static bool ProcessCheat(Player player, string msg) {
		switch (msg) {
			case "operation cwal":
				foreach (string id in player.model.ModelIds) {
					player.model.GetModel(id).constructionTime = 5;
				}
				foreach (Unit u in player.units) {
					if (u is Building) {
						(u as Building).CompleteNow();
					}
				}
				return true;
			case "show me the money":
				player.gold += 10000;
				return true;
			case "power overwhelming":
				foreach (string id in player.model.ModelIds) {
					player.model.GetModel(id).armor = 100;
				}
				return true;
			case "food for thought":
				foreach (string id in player.model.ModelIds) {
					player.model.GetModel(id).supplyCost = 0;
				}
				return true;
			case "something for nothing":
				foreach (string id in player.model.ModelIds) {
				//	player.model.GetModel(id).damage += player.model.GetModel(id).damage * 15 / 30;
					player.model.GetModel(id).armor += 3;
				}
				return true;
			case "black sheep wall":
				Game.fow.isEnabled = false;
				return true;
		}
		return false;
	}
}
