  Shader "Decal Transparent" {
    Properties {
		_Color ("Main Color", Color) = (1,1,1,0.5)
		_DecalColor ("Decal Color", Color) = (1,1,1,0.5)
		_MainTex ("Texture", 2D) = "white" { }
		_DecalTex ("Decal", 2D) = "white" { }
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
      Tags { "RenderType" = "Transparent" }
      CGPROGRAM
      #pragma surface surf TranspSurf alphatest:_Cutoff

	half4 LightingTranspSurf(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten) {
          half3 h = normalize (lightDir + viewDir);

          half NdotL = dot (s.Normal, lightDir);
          half diff = NdotL * 0.5 + 0.5;

          float nh = max (0, dot (s.Normal, h));
          float spec = pow (nh, 48.0);

          half4 c;
          c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * (atten * 2);
          c.a = s.Alpha;
          return c;
      }

      struct Input {
          float2 uv_MainTex;
		  float3 viewDir;
      };
      sampler2D _MainTex;
      sampler2D _DecalTex;
	  float4 _Color;
	  float4 _DecalColor;
      void surf (Input IN, inout SurfaceOutput o) {
		half4 texcol = tex2D (_MainTex, IN.uv_MainTex);
		half4 deccol = tex2D (_DecalTex, IN.uv_MainTex) * _DecalColor;
		deccol.xyz *= deccol.w;
		o.Albedo = (1.0f - deccol.w) * texcol * _Color + deccol;
		o.Alpha = texcol.w;
      }
      ENDCG
    } 
    Fallback "Diffuse"
  }
