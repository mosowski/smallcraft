﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapReader{

	public static string[] blockedPatterns = { "X", "W" };

	public static MapTile[,] GetObstaclesMap(int w, int h) {
		MapTile[,] result = new MapTile[w+2,h+2];
		for (int i = 0; i < w+2; ++i) {
			for (int j = 0; j < h+2; ++j) {
				result[i,j] = new MapTile(i,j);
				if (i == 0 || j == 0 || i == w + 1 || j == h+1) {
					result[i,j].extraCost = 1000;
				}
			}
		}
		Object.Destroy(GameObject.Find("StartingLocation1").GetComponent<MeshRenderer>());
		Object.Destroy(GameObject.Find("StartingLocation2").GetComponent<MeshRenderer>());
		Object.Destroy(GameObject.Find("StartingLocation3").GetComponent<MeshRenderer>());
		Object.Destroy(GameObject.Find("StartingLocation4").GetComponent<MeshRenderer>());

		GameObject mapObj = GameObject.Find("Map");
		foreach (Transform t in mapObj.transform) {
			string name = t.gameObject.name;
			int tileCol = (int)(t.position.x/2)*2;
			int tileRow = (int)(t.position.z/2)*2;
			foreach (string blockedPattern in blockedPatterns) {
				if (name.IndexOf(blockedPattern) >= 0) {
					for (int i = tileCol + 1; i < tileCol + 3; ++i) {
						for (int j = tileRow + 1; j < tileRow + 3; ++j) {
							result[i,j].extraCost = 1000;
						}
					}
				}
			}
			if (name.IndexOf('H') >= 0) {
				for (int i = tileCol + 1; i < tileCol + 3; ++i) {
					for (int j = tileRow + 1; j < tileRow + 3; ++j) {
						result[i,j].height = 1.2f;
						result[i,j].terrainType = TerrainType.High;
					}
				}
			} else if (name.IndexOf('L') >= 0) {
				for (int i = tileCol + 1; i < tileCol + 3; ++i) {
					for (int j = tileRow + 1; j < tileRow + 3; ++j) {
						if (result[i,j].terrainType != TerrainType.High) {
							result[i,j].terrainType = TerrainType.Low;
						}
					}
				}
			} else if (name.IndexOf('R') >= 0) {
				float angle = t.rotation.eulerAngles.y;
				for (int i = tileCol + 1; i < tileCol + 3; ++i) {
					for (int j = tileRow + 1; j < tileRow + 3; ++j) {
						result[i,j].SetRampVertices(angle, i - tileCol - 1, j - tileRow - 1, 0, 1.2f);
					}
				}

			}
		}
		return result;
	}

	public static IEnumerable<Vector2> GetMinerals() {
		GameObject crystalsContainer = GameObject.Find("Map/Crystals");
		foreach (Transform t in crystalsContainer.transform) {
			Object.Destroy(t.gameObject.GetComponent<MeshRenderer>());
			yield return new Vector2(t.position.x, t.position.z);
		}
	}
}
