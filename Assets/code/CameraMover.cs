﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {

	GameObject marquee;

	public float threshold = 0.02f;
	public float speed = 1.0f;

	float clickThreshold = 0.2f;
	float lastMouseKeyDown = 0;

	public float minX;
	public float minZ;
	public float maxX;
	public float maxZ;

	public Texture marqueeTexture;
	public bool isMarqueeing;
	public float marqueeStartX;
	public float marqueeStartY;
	public float marqueeEndX;
	public float marqueeEndY;

	public float marqueeMinX;
	public float marqueeMinY;
	public float marqueeMaxX;
	public float marqueeMaxY;

	public bool isInConstructionMode;
	public UnitModel buildingModel;
	public string buildingModelName;
	public GameObject buildingPreview;

	public static Vector2 screenTL;
	public static Vector2 screenTR;
	public static Vector2 screenBR;
	public static Vector2 screenBL;

	void Awake() {
		marqueeTexture = Resources.Load<Texture>("2d/marquee");
		threshold = 0.004f;

		minX = 0;
		minZ = 0;
		maxX = 128;
		maxZ = 128;

		transform.position = new Vector3(transform.position.x, 15, transform.position.z);
	}

	void Update () {
		if (!Game.isPlaying) {
			return;
		}

		if (!isMarqueeing) {
			UpdateScroll();
		}

		if (Game.me != null) {
			UpdateMarquee();
		}

		if (isInConstructionMode) {
			UpdateConstructionMode();
		}

		UpdateScreenRect();
	}

	private void UpdateScroll() {
		Vector3 position= transform.position;
		if (Input.mousePosition.x < Screen.width * threshold) {
			position.x += -speed;
			position.z += -speed;
		}
		if (Input.mousePosition.x > Screen.width * (1.0f - threshold)) {
			position.x += speed;
			position.z += speed;
		}

		if (Input.mousePosition.y < Screen.width * threshold) {
			position.z += -speed;
			position.x += speed;
		}
		if (Input.mousePosition.y > Screen.height - Screen.width * threshold) {
			position.z += speed;
			position.x += -speed;
		}

		position.x = Mathf.Min(Mathf.Max(position.x, minX), maxX);
		position.z = Mathf.Min(Mathf.Max(position.z, minZ), maxZ);
		transform.position = position;
	}

	private void UpdateMarquee() {
		if (Input.GetMouseButtonDown(0)) {

			marqueeStartX = Input.mousePosition.x;
			marqueeStartY = Input.mousePosition.y;

			if (SWGUI.isOutsideHUD(marqueeStartX, Camera.main.pixelHeight - marqueeStartY)) {
				lastMouseKeyDown = Time.time;
			}
		}

		if (lastMouseKeyDown != 0 && Time.time - lastMouseKeyDown > clickThreshold) {
			isMarqueeing = true;
		}

		if (isMarqueeing) {
			float marqueX = Input.mousePosition.x;
			float marqueY = Input.mousePosition.y;

			marqueeMinX = Mathf.Min(marqueeStartX, marqueX);
			marqueeMaxX = Mathf.Max(marqueeStartX, marqueX);
			marqueeMinY = Mathf.Min(marqueeStartY, marqueY);
			marqueeMaxY = Mathf.Max(marqueeStartY, marqueY);

			if (Input.GetMouseButtonUp(0)) {
				Vector2 v1 = GetGamePostion(marqueeMinX, marqueeMinY);
				Vector2 v2 = GetGamePostion(marqueeMaxX, marqueeMinY);
				Vector2 v3 = GetGamePostion(marqueeMaxX, marqueeMaxY);
				Vector2 v4 = GetGamePostion(marqueeMinX, marqueeMaxY);

				Actions.QueueActionSelect(v1, v2, v3, v4);
				ClientMagicBox.Cancel();
				isMarqueeing = false;
			}
		} else if (Input.GetMouseButtonUp(0)) {
			OnLMBClick(Input.mousePosition.x, Input.mousePosition.y);
		}

		if (Input.GetMouseButtonUp(0)) {
			lastMouseKeyDown = 0;
		}

		if (Input.GetMouseButtonUp(1)) {
			OnRMBClick(Input.mousePosition.x, Input.mousePosition.y);
		}

		if (Input.GetKeyDown(KeyCode.A)) {
			ClientMagicBox.AttackMode();
		}

		if (Input.GetKeyDown(KeyCode.S)) {
			ClientMagicBox.AttackMoveMode();
		}

		if (Input.GetKeyDown(KeyCode.Q)) {
			ClientMagicBox.MoveMode();
		}

		if (Input.GetKeyDown(KeyCode.W)) {
			ClientMagicBox.Stop();
		}

		if (Input.GetKeyDown(KeyCode.Z)) {
			ClientMagicBox.GatherMode();
		}

		if (Input.GetKeyDown(KeyCode.X)) {
			ClientMagicBox.BuildMode();
		}

		//if (Input.GetKeyDown(KeyCode.Return)) {
		//	ClientMagicBox.ToggleChatBox();
		//}
	}

	void UpdateConstructionMode() {
		Vector2 vec = GetGamePostion(Input.mousePosition.x, Input.mousePosition.y);
		if (vec.x >= buildingModel.tileWidth && vec.y >= buildingModel.tileHeight && vec.x < Game.mapSize - buildingModel.tileWidth && vec.y < Game.mapSize - buildingModel.tileHeight) {
			Vector2 clamped = buildingModel.GetClampedCoords(vec);
			buildingPreview.transform.position = new Vector3(clamped.x, Game.GetMapTile(vec).height, clamped.y);

			if (Input.GetMouseButtonDown(0)) {
				if (Game.CanPlaceBuilding(vec, buildingModel)) {
					if (Game.me.gold >= buildingModel.cost) {
						int col, row;
						buildingModel.GetMapCoords(vec, out col, out row);
						Actions.QueueActionPlaceBuilding(buildingModelName, col, row);
						AudioManager.PlaySFX("buildingPlaced", Vector3.zero);
					} else {
						Notifications.Add("To build " + buildingModel.displayName + " you need " + buildingModel.cost+ " minerals");
					}
				} else {
					Notifications.Add("Cannot build here");
				}
				ExitConstructionMode();
			}
		}
	}

	void OnLMBClick(float x, float y) {
		if (SWGUI.isOutsideHUD(x, Camera.main.pixelHeight - y)) {
			Vector2 vec = GetGamePostion(x, y);
			if (Game.IsInGameArea(vec)) {
				ClientMagicBox.OnLMBClick(vec);
			}
		}
	}

	void OnRMBClick(float x, float y) {
		if (SWGUI.isOutsideHUD(x, Camera.main.pixelHeight - y)) {
			Vector2 vec = GetGamePostion(x, y);
			if (Game.IsInGameArea(vec)) {
				ClientMagicBox.OnRMBClick(vec);
			}
		}
	}

	void OnGUI() {
		if (isMarqueeing) {
			GUI.DrawTexture(new Rect(marqueeMinX, Camera.main.pixelHeight - marqueeMinY, marqueeMaxX - marqueeMinX, - marqueeMaxY + marqueeMinY), marqueeTexture);
		}
	}

	Vector2 GetGamePostion(float x, float y) {
		Vector2 groundClick = GetGamePostion(x, y, 0.5f);
		MapTile tile = Game.SafeGetMapTile(groundClick);
		if (tile != null) {
			return GetGamePostion(x, y, tile.height);
		} else {
			return groundClick;
		}
	}

	Vector2 GetGamePostion(float x, float y, float height) {
		Ray ray = GetComponent<Camera>().ScreenPointToRay(new Vector3(x, y, 0));
		Plane xz = new Plane(Vector3.up, Vector3.up * height);
		float d = 0;
		if (xz.Raycast(ray, out d)) {
			Vector3 point = ray.GetPoint(d);
			return new Vector2(point.x, point.z);
		}
		return Vector2.zero;
	}

	public void EnterConstructionMode(string buildingModelName) {
		if (isInConstructionMode) {
			ExitConstructionMode();
		}

		isInConstructionMode = true;
		buildingPreview = Factory.MakeBuildingPreview(buildingModelName);
		this.buildingModelName = buildingModelName;
		buildingModel = Game.me.model.GetModel(buildingModelName);
	}

	public void ExitConstructionMode() {
		isInConstructionMode = false;
		Object.Destroy(buildingPreview);
	}

	public void UpdateScreenRect() {
		if (Game.map != null) {
			screenTL = GetGamePostion(0, 0);
			screenTR = GetGamePostion(Camera.main.pixelWidth, 0);
			screenBR = GetGamePostion(Camera.main.pixelWidth, Camera.main.pixelHeight);
			screenBL = GetGamePostion(0, Camera.main.pixelHeight);

			Vector3 position = transform.position;
			float minX = Mathf.Min(screenTL.x, screenBL.x);
			float maxX = Mathf.Max(screenTR.x, screenBR.x);
			float minY = Mathf.Min(screenTL.y, screenBL.y);
			float maxY = Mathf.Max(screenTR.y, screenBR.y);
			if (minX < 0) {
				position.x += -minX;
			}
			if (minY < 0) {
				position.z += -minY;
			}
			if (maxX > Game.mapSize) {
				position.x -= (maxX - Game.mapSize);
			}
			if (maxY > Game.mapSize) {
				position.z -= (maxY - Game.mapSize);
			}
			//transform.position = position;
		}
	}

	public static void LookAt(Vector2 position) {
		Camera.main.transform.position = new Vector3(position.x, Camera.main.transform.position.y, position.y);
	}
}
