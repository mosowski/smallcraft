﻿using UnityEngine;
using System.Collections;

public class BaseUnitComponent : MonoBehaviour {

	protected Unit unit;
	protected Vector2 direction;
	protected GameObject body;
	protected bool isVisible;

	protected GameObject selectionMarker;
	protected bool isSelected;


	public virtual void Setup(Unit unit) {
		this.unit = unit;
		direction = Vector2.zero;
		isVisible = true;
	}

	protected virtual void DestroyBody() {
		if (body != null) {
			body.transform.parent = null;
			GameObjectPool.Release(body);
			body = null;
		}
	}

	protected virtual void AttachBody() {
		if (body != null) {
			body.transform.parent = transform;
			body.transform.localPosition = Vector3.zero;
			body.transform.localRotation = Quaternion.identity;
			body.SetActive(isVisible);
		}
	}	

	protected virtual void SetBodyMaterial() {
		if (unit.player != Game.neutral && body != null) {
			SetGameObjectPlayerMaterialRecursive(body);
		}
	}

	protected void SetGameObjectPlayerMaterialRecursive(GameObject obj) {
		if (obj.renderer != null && obj.renderer.material != null) {
			obj.renderer.material = MaterialManager.GetMaterialForPlayer(obj.renderer.material, unit.player);
		}
		foreach (Transform t in obj.transform) {
			SetGameObjectPlayerMaterialRecursive(t.gameObject);
		}
	}

	protected bool IsVisible {
		get {
			return isVisible;
		}
		set {
			if (body != null) {
				body.SetActive(value);
			}
			isVisible = value;
		}
	}

	protected virtual Vector3 UnitPosition {
		get {
			return new Vector3(unit.position.x, Game.GetMapHeight(unit.position) + unit.HeightOffset, unit.position.y);
		}
	}

	protected virtual void SetPositionImmediately() {
		transform.position = UnitPosition;
	}


	protected virtual void UpdatePosition() {
		float step =  Mathf.Min(1.0f, 5.0f * Time.deltaTime);
		transform.position += (UnitPosition - transform.position) * step;
	}

	protected virtual void UpdateDirection() {
		float step =  Mathf.Min(1.0f, 5.0f * Time.deltaTime);
		direction += (unit.direction - direction) * step;
		if (direction != Vector2.zero) {
			transform.eulerAngles = new Vector3(0, Mathf.Atan2(direction.y, -direction.x) * Mathf.Rad2Deg, 0);
		}
	}
	
	protected virtual void UpdateSelectionMarker() {
		if (!unit.isDead) {
			if (unit.isSelected != isSelected) {
				if (unit.isSelected) {
					if (Game.me.selection.Contains(unit)) {
						if (selectionMarker == null) {
							selectionMarker = SelectionMarkerManager.CreateSelectionMarker(unit);
							selectionMarker.transform.parent = transform;
							selectionMarker.transform.localPosition = new Vector3(0,0.1f,0);
						}
					} else {
						RemoveSelectionMarker();
					}
				} else {
					RemoveSelectionMarker();
				}
				isSelected = unit.isSelected;
			}
		} else {
			RemoveSelectionMarker();
		}
	}

	protected virtual void RemoveSelectionMarker() {
		if (selectionMarker != null) {
			selectionMarker.transform.parent = null;
			SelectionMarkerManager.ReleaseSelectionMarker(selectionMarker);
			selectionMarker = null;
		}
	}

	protected virtual void UpdateGameObject() {
		if (unit.isDead) {
			if (unit.carriedObject != null) {
				GameObjectPool.Release(unit.gameObject);
				unit.carriedObject = null;
			}
			GameObjectPool.Release(gameObject);
		}
	}

	protected virtual void DrawHUD() {
		Vector3 screenPos = Camera.main.WorldToScreenPoint(unit.gameObject.transform.position + new Vector3(0,0.8f,0));
		float width = 100 * unit.model.smallRadius;
		float health = (float)unit.hp / unit.model.maxHp;
		GUI.DrawTexture(new Rect(screenPos.x - width/2, Camera.main.pixelHeight - screenPos.y - width/2, width * health, 7), CommonAssets.healthBarFg);
		GUI.DrawTexture(new Rect(screenPos.x - width/2 + width * health, Camera.main.pixelHeight - screenPos.y - width/2, width * (1 - health), 7), CommonAssets.healthBarBg);

		if (unit.IsProducing) {
			float production = 1.0f - (float)unit.CurrentProductionLeftTime / unit.CurrentProductionTotalTime;
			GUI.DrawTexture(new Rect(screenPos.x - width/2, Camera.main.pixelHeight - screenPos.y - width/2 + 10, width * production, 7), CommonAssets.productionBarFg);
		}
	}

	void OnGUI() {
		if (!unit.isDead) {
			if (unit.isSelected && unit.player == Game.me || unit.lastDamageTick + 26 > Game.currentTickNum) {
				DrawHUD();			
			}
		}
	}
}
