﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BinaryHeap<W,T> where W : IComparable {

	private T[] items;
	private W[] weights;
	private int index;

	public BinaryHeap(int maxSize) {
		items = new T[maxSize+1];
		weights = new W[maxSize+1];
		index = 1;
	}

	public void Clear() {
		for (int i = 0; i < index; ++i ) {
			items[i] = default(T);
		}
		index = 1;
	}

	public void Push(W weight, T item) {
		items[index] = item;
		weights[index] = weight;

		int i = index;
		int j = i/2;
		while (i != 1 && weights[j].CompareTo(weight) > 0) {
			Swap(i, j);
			i = j;
			j /= 2;
		}

		index++;
	}

	public T Pop() {
		index--;
		T top = items[1];
		items[1] = items[index];
		weights[1] = weights[index];
		items[index] = default(T);

		int i = 1, j = 0;
		while (j != i) {
			j = i;
			if (i*2 < index && weights[j].CompareTo(weights[i*2]) > 0) {
				j = i*2;
			}
			if (i*2+1< index && weights[j].CompareTo(weights[i*2+1]) > 0) {
				j = i*2+1;
			}
			if (i != j) {
				Swap(i, j);
				i = j;
				j = 0;
			}
		}
		return top;
	}

	public void Swap(int i, int j) {
		T item = items[j];
		W weight = weights[j];
		items[j] = items[i];
		weights[j] = weights[i];
		items[i] = item;
		weights[i] = weight;
	}


	public T Top {
		get { return items[index - 1]; }
	}

	public int Count {
		get { return index - 1; }
	}
}

public static class AStar {

	public class Node {
		public int x;
		public int y;
		public float extraCost;

		public bool isVisited;
		public float currentCost;
		public Node parent;
	}

	private static BinaryHeap<float,Node> heap = new BinaryHeap<float,Node>(65536);
	private static Node[] result = new Node[16384];

	public static IEnumerable<Node> GridPath(Node[,] grid, int x1, int y1, int x2, int y2, int stepsLimit = -1) {
		int width = grid.GetLength(0);
		int height = grid.GetLength(1);

		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				grid[i,j].isVisited = false;
				grid[i,j].parent = null;
			}
		}

		heap.Clear();
		heap.Push(0, grid[x1,y1]);

		while (heap.Count > 0) {
			Node node = heap.Pop();
			if (!node.isVisited) {
				node.isVisited = true;
				if (node.x == x2 && node.y == y2) {
					int resultPtr = 0;
					result[resultPtr++] = node;
					while (node.x != x1 || node.y != y1) {
						node = node.parent;
						result[resultPtr++] = node;
					}
					for (int i = resultPtr - 1; i >= 0; --i) {
						yield return result[i];
						result[i] = null;
					}
					break;
				} else if (node.x > 0 && node.y > 0 && node.x < width - 1 && node.y < height - 1) {
					for (int i = -1; i <= 1; ++i) {
						for (int j = -1; j <= 1; ++j) {
							if (i != 0 || j != 0) {
								Node nb = grid[node.x + i, node.y + j];
								if (!nb.isVisited) {
									float newCost = node.currentCost + Mathf.Sqrt(i*i+j*j) + nb.extraCost;
									float estimatedCost = newCost + Mathf.Sqrt((nb.x-x2)*(nb.x-x2)+(nb.y-y2)*(nb.y-y2));
									if (nb.parent == null || newCost < nb.currentCost) {
										nb.currentCost = newCost;
										nb.parent = node;
										heap.Push(estimatedCost, nb);
									}
								}
							}
						}
					}
				}
				stepsLimit--;
				if (stepsLimit == 0) {
					break;
				}
			}
		}
	}
}
