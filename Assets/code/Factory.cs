﻿using UnityEngine;
using System.Collections;

public class Factory {

	public static MobileUnit MakeUnit(string modelName, Vector2 position, Player owner) {
		MobileUnit unit = new MobileUnit();
		UnitModel uModel = owner.model.GetModel(modelName);

		unit.position = position;
		unit.Setup(uModel);

		Game.AddUnit(unit);
		owner.AddUnit(unit);

		unit.gameObject = GameObjectPool.Create("units/" + modelName);
		unit.gameObject.GetComponent<UnitComponent>().Setup(unit);
		return unit;
	}

	public static Building MakeBuilding(string modelId, Vector2 position, Player owner) {
		int col, row;
		UnitModel model = owner.model.GetModel(modelId);
		model.GetMapCoords(position, out col, out row);
		return MakeBuilding(modelId, col, row, owner);
	}

	public static Building MakeBuilding(string modelId, int col, int row, Player owner) {
		Building unit = new Building();
		UnitModel buildingModel = owner.model.GetModel(modelId);

		unit.anchorCol = col;
		unit.anchorRow = row;
		unit.position.x = unit.anchorCol + buildingModel.tileWidth * 0.5f - 1;
		unit.position.y = unit.anchorRow + buildingModel.tileHeight * 0.5f - 1;

		unit.Setup(buildingModel);

		Game.AddUnit(unit);
		owner.AddUnit(unit);

		unit.gameObject = GameObjectPool.Create("buildings/" + modelId);
		unit.gameObject.GetComponent<BuildingComponent>().Setup(unit);
		return unit;
	}

	public static GameObject MakeBuildingPreview(string modelId) {
		GameObject preview = Object.Instantiate(Resources.Load<GameObject>("buildings/body/" + modelId)) as GameObject;
		return preview;
	}

	public static Mineral MakeMineral(Vector2 position) {
		Mineral unit = new Mineral();
		UnitModel model = Game.neutral.model.GetModel("Mineral");

		unit.position = position;
		unit.minerals = 1500;
		unit.Setup(model);

		Game.AddUnit(unit);
		Game.neutral.AddUnit(unit);

		unit.gameObject = GameObjectPool.Create("units/Mineral");
		unit.gameObject.GetComponent<MineralComponent>().Setup(unit);
		return unit;
	}
}
