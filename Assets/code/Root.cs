﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;

public class Root : MonoBehaviour {

	public static Client client;

	float lastTickTime;

	int currentTick = 0;
	int currentTurn = 1;
	int localTick = 0;

	public bool turnReady { get; private set; }
	public bool turnReported {get; private set; }

	public static bool active = true;

	void Awake() {
		Lobby.Init();
		CommonAssets.Init();
		client = new Client();
		client.Connect();
		MusicManager.PlayMenu();
	}

	public void StartGame(JsonData pNames) {
		List<string> playerNames = new List<string>();
		for(int i=0;i<pNames.Count;i++) {
			playerNames.Add((string)pNames[i]);
		}
		Game.Init(playerNames, Lobby.playerName);
		MusicManager.PlayGame();
	}

	public static void StopGame() {
		active = false;
	}

	public static void ReportMatchResult(IEnumerable<string> victors, IEnumerable<string> victims) {
		client.Call("reportMatchResult", victors, victims);
	}

	void Update () {
		foreach (JsonData command in client.Tick()) {
			ProcessCommand(command);
		}

		if (Game.isPlaying) {
			if (Time.time > lastTickTime + 0.04f && localTick < 5) {
				Game.Tick();
				lastTickTime = Time.time;
				localTick++;
				currentTick++;
			}

			if (localTick == 5) {
				if (!turnReported) {
					if (!active) {
						Game.queuedActions.Clear();
					}
					client.ReportTurn(currentTurn, Game.queuedActions);
					Game.queuedActions.Clear();
					turnReported = true;
				}
				if (turnReady) {
					localTick = 0;
					turnReported = false;
					turnReady = false;
					currentTurn++;
				}
			}
		}
	}

	void OnGUI() {
		SWGUI.OnTick (this, client);
	}

	private void NewTurn(JsonData turn) {
		turnReady = true;
		for(int i=0;i<turn.Count;i++) {
			Game.actions.Add(turn[i]);
		}
	}

	void ProcessCommand(JsonData command) {
		string m = (string)command["m"];

		switch(m) {
			case "turn":
				NewTurn(command["args"][0]);			
			break;
			case "gameStarted":
				StartGame(command["args"][0]);
				Lobby.OnGameStarted();
			break;
			case "loginOk":
				Lobby.OnLoginOk();
				break;
			case "loginFailed":
				Lobby.OnLoginFailed((string)command["args"]["error"]);
				break;
			case "playerJoinedRoom":
				Lobby.OnPlayerJoinedRoom((string)command["args"]["roomName"], (string)command["args"]["player"]);
				break;
			case "playerLeftRoom":
				if (Game.isPlaying) {
					string playerName = (string)command["args"]["player"];
					Game.PlayerDisconnected(Game.GetPlayer(playerName));
				} else {
					Lobby.OnPlayerLeftRoom((string)command["args"]["roomName"], (string)command["args"]["player"]);
				}
				break;
			case "roomsList":
				Lobby.OnSetRooms(command["args"]);
				break;
			case "ranking":
				Lobby.OnRankingReceived(command["args"]);
				break;
			case "globalStats":
				Lobby.numPlayersOnline = (int)command["args"]["numPlayersOnline"];
				break;
			case "roomFull":
				Lobby.OnRoomFull();
				break;
			case "roomNotFull":
				// do nothing..
				break;
			default: 
				Debug.Log("unknown command: " + m);
			break;
		}
	}
}
