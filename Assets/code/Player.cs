﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player {
	public string name;
	public int number;

	public int gold = 50;

	public bool isHuman = false;

	public Model model;
	public List<Unit> units;
	public List<Unit> selection;
	public Dictionary<string,int> unitCountByModelId;
	public int providedSupply;
	public int usedSupply;

	public Player() {
		units = new List<Unit>();
		selection = new List<Unit>();
		model = new Model();

		unitCountByModelId = new Dictionary<string,int>();
		foreach (string modelId in model.ModelIds) {
			unitCountByModelId[modelId] = 0;
		}
	}

	public void AddUnit(Unit unit) {
		units.Add(unit);
		unit.player = this;
		providedSupply += unit.model.providedSupply;
		usedSupply += unit.model.supplyCost;
		unitCountByModelId[unit.model.id]++;
	}

	public void RemoveUnit(Unit unit) {
		units.Remove(unit);
		providedSupply -= unit.model.providedSupply;
		usedSupply -= unit.model.supplyCost;
		unitCountByModelId[unit.model.id]--;
	}

	public void BuildStartingBase(Vector2 startingLocation) {
		Factory.MakeBuilding("CommandCenter", startingLocation, this).CompleteNow();
		Quaternion quat = Quaternion.AngleAxis(10, Vector3.forward);
		Vector3 spawnVector = new Vector3(3.5f, 0, 0);
		for (int i = 0 ; i < 5; ++i) {
			spawnVector = quat * spawnVector;
			Factory.MakeUnit("Worker", startingLocation + new Vector2(spawnVector.x, spawnVector.y), this);
		}
	}

	public void ClickSelect(Vector2 v) {
		Unit nearest = Game.GetNearestUnit(v);

		ClearSelection();
		if (nearest != null && units.Contains(nearest)) {
			selection.Add(nearest);
			nearest.IsSelected = true;
		}
	}

	public void ClearSelection() {
		for (int i = 0; i < selection.Count; ++i) {
			selection[i].IsSelected = false;
		}
		selection.Clear();
	}

	public void Select(Vector2 v1, Vector2 v2, Vector2 v3, Vector2 v4) {
		ClearSelection();

		int minX = Game.ClampToBounds((int)Mathf.Min(Mathf.Min(v1.x, v2.x), Mathf.Min(v3.x, v4.x)));
		int minY = Game.ClampToBounds((int)Mathf.Min(Mathf.Min(v1.y, v2.y), Mathf.Min(v3.y, v4.y)));
		int maxX = Game.ClampToBounds((int)Mathf.Max(Mathf.Max(v1.x, v2.x), Mathf.Max(v3.x, v4.x)));
		int maxY = Game.ClampToBounds((int)Mathf.Max(Mathf.Max(v1.y, v2.y), Mathf.Max(v3.y, v4.y)));
		Vector2[] poly = new Vector2[] { v1, v2, v3, v4 };

		bool hasUnit = false;

		for (int i = minX; i <= maxX; ++i) {
			for (int j = minY; j <= maxY; ++j) {
				MapTile tile = Game.GetMapTile(i + 1, j + 1);
				foreach (Unit unit in tile.units) {
					if (PointInPoly.Check(unit.position, poly)) {
						selection.Add(unit);
						if (unit is MobileUnit) {
							hasUnit = true;
						}
					}
				}
			}
		}

		if (hasUnit) {
			selection.RemoveAll( unit => unit is Building);
			selection.RemoveAll( unit => !units.Contains(unit));
		}
		foreach (Unit unit in selection) {
			unit.IsSelected = true;
		}
	}

	public void Tick() {
		selection.RemoveAll(unit => unit.isDead);

		for (int i = units.Count - 1; i >= 0; --i) {
			if (units[i].isDead) {
				RemoveUnit(units[i]);
			}
		}

		if (isHuman && Game.playersDown.IndexOf(this) == -1) {
			bool tmp = false;
			foreach (string buildingModelId in model.buildingModelIds) {
				if (unitCountByModelId[buildingModelId] > 0) {
					tmp = true;
				}
			}
			if (!tmp) {
				Game.PlayerLost(this);
			}
		}
	}

	public bool CheckDependencies(string modelId) {
		foreach (string dependency in model.GetModel(modelId).productionDependencies) {
			if (unitCountByModelId[dependency] <= 0) {
				return false;
			}
		}
		return true;
	}

}
