﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;


public class Client { 

	const int READ_BUFFER_SIZE = 512;
	TcpClient client;
	byte[] readBuffer = new byte[READ_BUFFER_SIZE];
	string receivedData = "";
	Queue<string> writeQueue;

	// Use this for initialization
	public void Connect () {
		writeQueue = new Queue<string>();
		try {
			//string host = "178.239.138.93";
			string host = "127.0.0.1";
			int hostPort = 4242;
			int crossdomainPort = 4243;

			Debug.Log("WebPlayer. Looking for cross-domain");
			if (Security.PrefetchSocketPolicy(host, crossdomainPort)) {
				client = new TcpClient(host, hostPort);
			}

			if (client != null) {
				Debug.Log("Connection Succeeded");
			} else {
				Debug.Log("Connection Failed");
			}
		} 
		catch(Exception ex) {
			Debug.Log("Server is not active.  Please start server and try again.      " + ex.ToString());
		}
	}

	public void Disconnect() {
		client.Close();
	}

	public IEnumerable<JsonData> Tick() {
		if (client != null && client.Connected) {
			NetworkStream stream = client.GetStream();
			StreamWriter writer = new StreamWriter(stream);

			while (writeQueue.Count > 0) {
				writer.Write(writeQueue.Dequeue() + "#");
			
			}
			writer.Flush();

			if (stream.CanRead) {
				while (stream.DataAvailable) {
					int bytesRead = stream.Read(readBuffer, 0, READ_BUFFER_SIZE);
					receivedData += Encoding.ASCII.GetString(readBuffer, 0, bytesRead);
				}
			}
			string[] splitted = receivedData.Split('#');

			for (int i = 0;i<splitted.Length - 1;i++) {
				Debug.Log(splitted[i]);
				yield return JsonMapper.ToObject(splitted[i]);
			}

			receivedData = splitted[splitted.Length - 1];
		}
	}

	public void ReportTurn(int turnNumber, List<Hashtable> turns) {
		Call("reportTurn", turnNumber, turns);
	}

	public void Call(string method, params object[] args) {
		Hashtable message = new Hashtable();
		message.Add("m", method);
		message.Add("args", args);
		string json = JsonMapper.ToJson(message);
		Send(json);
	}

	void Send(string data) {
		writeQueue.Enqueue(data);
	}
	
}
