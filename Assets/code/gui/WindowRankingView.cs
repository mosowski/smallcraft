﻿using UnityEngine;
using System.Collections;

public class WindowRankingView : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Lobby.RankingReceived += OnRankingReceived;
		Lobby.GetRanking();
		UIEventListener.Get(transform.Find("CloseButton").gameObject).onClick += (go) => {
			AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
			Close();
		};

	}

	void OnRankingReceived() {
		GameObject ranking = transform.Find("RankingList").Find("Grid").gameObject;
		foreach (RankingItem item in Lobby.ranking) {
			GameObject window = NGUITools.AddChild(ranking, Resources.Load<GameObject>("gui/RankingRow"));
			window.transform.Find("Name").Find("Text").GetComponent<UILabel>().text = item.name;
			window.transform.Find("Score").Find("Text").GetComponent<UILabel>().text = item.score.ToString();
		}
		ranking.GetComponent<UIGrid>().Reposition();
		transform.Find("RankingList").GetComponent<UIScrollView>().ResetPosition();
	}

	void Close() {
		GUIManager.OpenWindow("WindowRooms");
		Lobby.RankingReceived -= OnRankingReceived;
		GUIManager.CloseWindow("WindowRanking");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
