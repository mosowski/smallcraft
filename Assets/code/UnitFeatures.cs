using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feature {
	public class Params {
	}

	public Unit unit { get; protected set; }

	public Feature(Unit unit) {
		this.unit = unit;
	}

	public virtual void Tick() {
	}

	public static Feature Create(Params p, Unit unit) {
		System.Type paramsType = p.GetType();
		return (Feature)paramsType.DeclaringType.GetConstructor(new System.Type[] { typeof(Unit), paramsType }).Invoke(new object[] { unit, p });
	}
}

public class MoveFeature : Feature {
	public new class Params : Feature.Params {
		public float maxSpeed;
		public float inertia;
	}

	public Params p;
	public bool isCollidable;

	public Vector2 boidFollow;
	public Vector2 boidAlign;
	public Vector2 boidAvoid;

	Vector2 boidSeparate;
	Vector2[] oldPositions;

	public MapTile targetTile;
	public Vector2 targetPoint;
	public List<Unit> leaders;
	public float reachRadius = 0;

	public MoveFeature(Unit unit, Params p) : base(unit) {
		this.p = p;
		oldPositions = new Vector2[5];
		isCollidable = true;
	}

	public override void Tick() {
		if (isCollidable) {
			Separate();
		}
		Steer();
		Move();
		UpdateAngle();
	}

	void Separate() {
		foreach(Unit u in Game.GetUnitsNear(unit.position)) {
			if (u == unit) {
				continue;
			}

			if (u.model.isFlying != unit.model.isFlying) {
				continue;
			}

			Vector2 delta = unit.position - u.position;
			if (delta == Vector2.zero) {
				delta = Vector2.right * 0.01f;
			}
			float distance = delta.magnitude;
			float minDistance = u.model.smallRadius + unit.model.smallRadius;
			if (distance < minDistance) {
				boidSeparate += delta.normalized * (minDistance - distance);
			}
		}
	}

	void Steer() {
		Vector2 steer = 
			boidFollow * 1.0f +
			boidAlign * 1.0f + 
			boidAvoid * 1.0f +
			boidSeparate * 4.0f;

		unit.velocity += (steer - unit.velocity) * p.inertia;
		if (unit.velocity.magnitude > p.maxSpeed) {
			unit.velocity = unit.velocity.normalized * p.maxSpeed;
		}

		boidFollow = Vector2.zero;
		boidAlign = Vector2.zero;
		boidAvoid = Vector2.zero;
		boidSeparate = Vector2.zero;
	}

	void Move() {
		unit.Tile.units.Remove(unit);
		if (unit.model.isFlying) {
			unit.position += unit.velocity;
		} else {
			if (!Game.IsWalkable(unit.position)) {
				//XXX
				unit.position += unit.velocity;
			} else {
				if (Game.IsWalkable(unit.position + unit.velocity)) {
					unit.position += unit.velocity;
				} else {
					if (Game.IsWalkable(unit.position + new Vector2(unit.velocity.x, 0))) {
						unit.position.x += unit.velocity.x;
					} else {
						unit.velocity.x = 0;
					}

					if (Game.IsWalkable(unit.position + new Vector2(0, unit.velocity.y))) {
						unit.position.y += unit.velocity.y;
					} else {
						unit.velocity.y = 0;
					}
				}
			}
		}
		unit.Tile.units.Add(unit);
	}

	void UpdateAngle() {
		if (unit.headDirection != Vector2.zero) {
			unit.direction = unit.headDirection;
			unit.headDirection = Vector2.zero;
		} else {
			if (unit.position != oldPositions[0]) {
				unit.direction = unit.position - oldPositions[0];
			}
		}
		for (int i = 0; i < oldPositions.Length - 1; ++i) {
			oldPositions[i] = oldPositions[i + 1];
		}
		oldPositions[oldPositions.Length - 1] = unit.position;
	}

	public bool IsMoving {
		get {
			return Vector2.Distance(oldPositions[0], unit.position) > 0.05f;
		}
	}
}

public class AttackFeature : Feature {
	public new class Params : Feature.Params {
		public int groundDamage;
		public float groundRange;
		public float groundSplashRange;
		public int groundDamageDelay;
		public string groundShotSound;
		public string groundProjectile;

		public int airRange;
		public int airDamage;
		public int airSplashRange;
		public int cooldown;
	}

	Params p;
	int lastFindTargetTick;
	int lastAttackTick;

	public Unit targetUnit;

	public AttackFeature(Unit unit, Params p) : base(unit) {
		this.p = p;
	}

	public Unit FindTargetInRange() {
		Unit nearest = null;
		float nearestDist = 0;
		if (Game.currentTickNum - lastFindTargetTick > 10) {
			foreach (Unit u in Game.GetUnitsInRadius(unit.position, (int)unit.model.sightRange)) {
				if (Game.IsEnemy(unit.player, u.player) && CanShoot(u)) {
					float dist = Vector2.Distance(u.position, unit.position);
					if (nearest == null || dist < nearestDist) {
						nearest = u;
						nearestDist = dist;
					}
				}
			}
			lastFindTargetTick = Game.currentTickNum;
		}
		return nearest;
	}

	public bool CanTarget(Unit target) {
		return ((target.model.isFlying && p.airDamage > 0) ||
			(!target.model.isFlying && p.groundDamage > 0));
	}

	public bool CanShoot(Unit target) {
		return CanTarget(target) && Vector2.Distance(unit.position, target.position) <= p.groundRange;
	}


	public void Shoot(Unit target) {
		unit.headDirection = target.position - unit.position;
		if (CanShoot(target)) {
			if (Game.currentTickNum > lastAttackTick + p.cooldown) {
				ProjectileManager.CreateProjectile(p.groundProjectile, unit, target);
				lastAttackTick = Game.currentTickNum;
				if (p.groundSplashRange != 0) {
					Game.DealSplashDamage(target.position, p.groundSplashRange, (int)p.groundRange, p.groundDamageDelay);
				} else {
					Game.DealDamage(target, p.groundDamage, p.groundDamageDelay);
				}
				PlayShootSound();
			}
		}
	}

	public bool HasShot {
		get {
			return lastAttackTick == Game.currentTickNum;
		}
	}

	public void PlayShootSound() {
		if (!string.IsNullOrEmpty(p.groundShotSound)) {
			AudioManager.PlaySFX(p.groundShotSound, unit.gameObject.transform.position);
		}
	}
}

public class ProductionFeature : Feature {
	public new class Params : Feature.Params {
		public string[] producedUnits;
	}

	public Params p;

	public ProductionFeature(Unit unit, Params p) : base(unit) {
		this.p = p;
	}
}

public class GatherFeature : Feature {
	public new class Params : Feature.Params {
	}

	public Vector2 targetPoint;
	public Vector2 depositPoint;
	public Unit depositUnit;
	public Mineral mineral;
	public int amountGathered;

	public GatherFeature(Unit unit, Params p) : base(unit) {
	}
}
