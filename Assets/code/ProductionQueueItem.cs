﻿using UnityEngine;
using System.Collections;

public class ProductionQueueItem {
	public Unit producer;
	public string itemName;
	public int numTicksLeft;
	public bool isCompleted;

	public ProductionQueueItem(Unit producer, string itemName) {
		this.producer = producer;
		this.itemName = itemName;
		numTicksLeft = ProductionTime;
	}

	public int ProductionTime {
		get {
			return producer.player.model.GetModel(itemName).constructionTime;
		}
	}

	public virtual void Tick() {
		numTicksLeft--;
		if (numTicksLeft <= 0) {
			OnCompleted();
		}
	}

	public virtual void OnCompleted() {
		isCompleted = true;
	}

	public virtual void OnQueued() {
	}

	public virtual void Cancel() {
	}
}
