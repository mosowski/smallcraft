﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIManager : MonoBehaviour {


	public static GUIManager instance;

	public static GameObject container;

	public static Dictionary<string, GameObject> windows;

	void Awake() {
		instance = this;
		windows = new Dictionary<string, GameObject>();
		container = transform.Find("Container").gameObject;
		OpenWindow("WindowLogin");
	}

	void Start () {
	
	}
	
	void Update () {
	
	}


	public static GameObject OpenWindow(string name) {
		Debug.Log(name);
		if (windows.ContainsKey(name)) {
			return null;
		}
		GameObject window = NGUITools.AddChild(container, Resources.Load<GameObject>("gui/windows/" + name));
		windows[name] = window;
		return window;
	}

	public static void CloseWindow(string name) {
		GameObject go = windows[name];
		if(go != null) {
			NGUITools.Destroy(go);
			windows.Remove(name);
		}
	}


}
