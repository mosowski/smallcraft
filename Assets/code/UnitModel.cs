﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitModel {
	public string id;
	public string displayName;
	public float clickRadius;
	public float smallRadius;
	public float sightRange;

	public float maxSpeed;
	public float inertia;

	public bool isFlying;
	public float flyingHeight;

	public bool isIndestructible;

	public int constructionTime;

	public int providedSupply;
	public int cost = 100;
	public int supplyCost;

	public int maxHp;
	public int maxEnergy;
	public int armor;

	public int tileWidth;
	public int tileHeight;
	public bool isDefensiveConstruction;

	public string destuctionPrefab;

	public List<Feature.Params> features;

	public List<string> productionDependencies = new List<string>();

	public List<string> acknowledegeSounds = new List<string>();

	public void GetMapCoords(Vector2 p, out int col, out int row) {
		col = (int)(p.x) - (int)(tileWidth * 0.5f) + 1;
		row = (int)(p.y) - (int)(tileHeight * 0.5f) + 1;
	}

	public Vector2 GetClampedCoords(Vector2 p) {
		int col = 0, row = 0;
		GetMapCoords(p, out col, out row);
		return new Vector2(col + tileWidth * 0.5f - 1, row + tileHeight * 0.5f - 1);
	}
}
