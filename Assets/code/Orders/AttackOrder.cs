using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class AttackOrder : MoveOrder {

	MapTile cachedTargetTile;
	AttackFeature attackFeature;

	public AttackOrder(Unit unit): base(unit) {
		attackFeature = unit.GetFeature<AttackFeature>();
	}

	public AttackOrder(Unit unit, Unit targetUnit): base(unit, targetUnit.position) {
		attackFeature = unit.GetFeature<AttackFeature>();
		attackFeature.targetUnit = targetUnit;
	}

	protected override MapTile GetTargetTile() {
		return Game.GetMapTile(attackFeature.targetUnit.position);
	}

	void ValidateTarget() {
		if (cachedTargetTile == null) {
			cachedTargetTile = GetTargetTile();
		}
		if (GetTargetTile().x != cachedTargetTile.x || GetTargetTile().y != cachedTargetTile.y) {
			cachedTargetTile = GetTargetTile();
			if (moveFeature.leaders.Contains(unit)) {
				InitCachedPath();
			}
		}
		moveFeature.targetPoint = attackFeature.targetUnit.position;
	}

	protected override void OnRechedDestination() {
		//don nothing;
	}

	protected virtual void OnTargetKilled() {
		unit.CancelOrder();
	}

	public override void Tick() {
		if (attackFeature.targetUnit.isDead) {
			OnTargetKilled();
			return;
		}
		ValidateTarget();

		if (attackFeature.targetUnit.isDead) {
			unit.CancelOrder();
		} else {
			Vector2 direction = attackFeature.targetUnit.position - unit.position;
			if (attackFeature.CanShoot(attackFeature.targetUnit)) {
				attackFeature.Shoot(attackFeature.targetUnit);
			} else {
				base.Tick();
			}
		}
	}
}

