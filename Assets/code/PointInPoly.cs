﻿using UnityEngine;
using System.Collections;

public static class PointInPoly {

	public static bool Check(Vector2 p, Vector2[] poly) {
		bool isIn = false;
		int k = poly.Length - 1;
		for (int i = 0; i < poly.Length; k = i++) {
			Vector3 a = poly[k];
			Vector3 b = poly[i];
			if ((p.y > a.y && p.y <= b.y) || (p.y > b.y && p.y <= a.y)) {
				float x = a.x + (b.x - a.x) * (p.y - a.y) / (b.y - a.y);
				if (x >= p.x) {
					isIn = !isIn;
				}
			}
		}
		return isIn;
	}
}
