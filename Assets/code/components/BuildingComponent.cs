﻿using UnityEngine;
using System.Collections;

public class BuildingComponent : BaseUnitComponent {

	public enum BodyState {
		Scaffolding,
		Complete,
		None
	}

	protected Building building;
	protected BodyState bodyState;

	public override void Setup(Unit unit) {
		base.Setup(unit);
		building = unit as Building;
		bodyState = BodyState.None;

		SetPositionImmediately();
		IsVisible = false;
	}

	protected virtual void SetScaffoldingState() {
		DestroyBody();
		body = GameObjectPool.Create("scaffoldings/" + unit.model.id);
		AttachBody();
		bodyState = BodyState.Scaffolding;
	}

	protected virtual void SetCompleteState() {
		DestroyBody();
		body = GameObjectPool.Create("buildings/body/" + unit.model.id);
		AttachBody();
		bodyState = BodyState.Complete;
	}

	protected virtual void UpdateBuilding() {
		if (!unit.isDead) {
			if (unit.Tile.fowState == FoWState.Visible) {
				if (building.IsComplete) {
				   	if (bodyState != BodyState.Complete) {
						SetCompleteState();
						SetBodyMaterial();
					}
				} else {
					if (bodyState != BodyState.Scaffolding) {
						SetScaffoldingState();
						SetBodyMaterial();
					}
				}

				if (!IsVisible) {
					IsVisible = true;
				}

				SetPositionImmediately();
			}
		}
	}

	void Update () {
		UpdateBuilding();
		UpdateGameObject();
	}
}
	
