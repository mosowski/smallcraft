using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ClientMagicBox {

	public static string chatText = "";

	public static Dictionary<string, bool> state = new Dictionary<string, bool>();

	public static List<Unit> selection = new List<Unit>();

	public static void OnRMBClick(Vector2 vec) {
		if (IsChanged()) {
			Cancel();
			return;
		}
		if (Game.me.selection.Count > 0) {
			Unit targetUnit = Game.GetNearestUnit(vec);
			if (targetUnit != null && !(targetUnit is Mineral) && !Game.me.units.Contains(targetUnit) && ClientMagicBox.CheckSelectionFor<AttackFeature>()) {
				Actions.QueueActionAttack(vec);
			} else if (CheckSelectionFor<GatherFeature>() && targetUnit is Mineral) {
				Actions.QueueActionGather(vec);
			} else if (CheckSelectionFor<MoveFeature>()) {
				Actions.QueueActionMove(vec);
			} else if (CheckSelectionFor<AttackFeature>()) {
				Actions.QueueActionAttackMove(vec);
			}
		}
	}
	
	public static void OnLMBDragOnMiniMap(Vector2 vec) {
		CameraMover.LookAt(vec);
	}

	public static void OnLMBClickFromMiniMap(Vector2 vec) {
		if (Get("attackMoveMode")) {
			OnLMBClick(vec);
		} else {
			CameraMover.LookAt(vec);
		}
	}

	public static void OnLMBClick(Vector2 vec) {
		Unit targetUnit = Game.GetNearestUnit(vec);
		if (Get("attackMode")) {
			if (targetUnit != null) {
				Actions.QueueActionAttack(vec);
			} 
			AttackMode();
		} else if (Get("moveMode")) {
			Actions.QueueActionMove(vec);
		} else if (Get("attackMoveMode")) {
			Actions.QueueActionAttackMove(vec);
			AttackMoveMode();
		} else if (Get("gatherMode")) {
			if (targetUnit is Mineral) {
				Actions.QueueActionGather(vec);
				GatherMode();
			}
		}  else {
			Cancel();
			Actions.QueueActionSelectClick(vec);
		}
	}

	public static void Toggle(string token) {
		if (state.ContainsKey(token)) {
			state[token] = !state[token];
		} else {
			state[token] = true;
		}
	}

	public static void ToggleChatBox() {
		if (Get("ChatBox")) {
			if(chatText != "") {
				Actions.QueueActionMsg(chatText);
			}
		
		} else {
			chatText = "";
		}
		Toggle("ChatBox");
	}

	public static bool Get(string token) {
		if (state.ContainsKey(token)) {
			return state[token];
		}
		return false;
	}

	public static void AttackMode() {
		if (Get("attackMode")) {
			Cancel();
			return;
		}
		if (CheckSelectionFor<AttackFeature>()) {
			Cancel();
			Toggle("attackMode");
		}
	}

	public static void MoveMode() {
		if (Get("moveMode")) {
			Cancel();
			return;
		}
		if (CheckSelectionFor<MoveFeature>()) {
			Cancel();
			Toggle("moveMode");
		}
	}

	public static void AttackMoveMode() {
		if (Get("attackMoveMode")) {
			Cancel();
			return;
		}
		if (CheckSelectionFor<AttackFeature>() && CheckSelectionFor<MoveFeature>()) {
			Cancel();
			Toggle("attackMoveMode");
		}
	}

	public static void Stop() {
		Actions.QueueActionStop();
	}

	public static void GatherMode() {
		if (Get("gatherMode")) {
			Cancel();
			return;
		}
		if (CheckSelectionFor<GatherFeature>()) {
			Cancel();
			Toggle("gatherMode");
		}
	}

	public static void BuildMode() {
		if (Get("buildMode")) {
			Cancel();
			return;
		}
		if (CheckSelectionFor<ProductionFeature>()) {
			Cancel();
			Toggle("buildMode");
		}
	}

	public static bool IsChanged() {
		foreach(bool ok in state.Values) {
			if (ok) {
				return true;
			}
		}
		return false;
	}

	public static void Spawn(string modelId) {
		UnitModel uModel = Game.me.model.GetModel(modelId);
		if (Game.me.gold < uModel.cost) {
			Notifications.Add("To build " + uModel.displayName + " you need " + uModel.cost + " minerals");
			return; 
		} else if (Game.me.usedSupply + uModel.supplyCost > Game.me.providedSupply) {
			Notifications.Add("To build " + uModel.displayName + " you need " + uModel.supplyCost + " supply");
			return;
		}
		Actions.QueueActionQueueProductionOrder(modelId);
	}

	public static void Cancel() {
		state = new Dictionary<string, bool>();
	}

	public static bool CheckSelectionFor<T>() where T : Feature {
		bool ok = true;
		if (Game.me.selection.Count == 0) {
			return false;
		}
		foreach(Unit unit in Game.me.selection) {
			ok = ok && unit.IsComplete && unit.GetFeature<T>() != null;
		}
		return ok;
	}
}
