using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class AttackMoveOrder : MoveOrder {

	protected AttackFeature attackFeature;

	public AttackMoveOrder(Unit unit): base(unit) {
		attackFeature = unit.GetFeature<AttackFeature>();
	}

	public override void Tick() {
		Unit found = attackFeature.FindTargetInRange();
		if (found != null) {
			unit.SetOrder(new SideAttackMoveOrder(unit, found, this));
		} else {
			base.Tick();
		}
	}

	public class SideAttackMoveOrder : AttackOrder {
		Order previousOrder;

		public SideAttackMoveOrder(Unit unit, Unit target, AttackMoveOrder previousOrder): base(unit, target) {
			this.previousOrder = previousOrder;
		}

		protected override void OnTargetKilled() {
			unit.SetOrder(previousOrder);
		}

	}
}
