﻿using UnityEngine;
using System.Collections;

public static class Notifications {

	class Item {
		public string text;
		public float duration;
	}

	static Item[] notifications;
	static GUIStyle textStyle = new GUIStyle();

	public static void Init() {
		notifications = new Item[10];
		textStyle.alignment =  TextAnchor.UpperLeft;
		textStyle.normal.textColor = Color.white;
		textStyle.onActive = textStyle.normal;
	}

	public static void Add(string text, float duration = 5.0f) {
		for (int i = 0; i < notifications.Length -1; ++i) {
			notifications[i] = notifications[i+1];
		}
		notifications[notifications.Length - 1] = new Item() { text = text, duration = duration};
	}

	public static void AddLocal(Player player, string text) {
		if (player == Game.me) {
			Add(text);
		}
	}

	public static void Draw(float scaleFactor) {
		string text = "";
		for (int i = 0; i < notifications.Length; ++i) {
			if (notifications[i] != null) {
				notifications[i].duration -= Time.deltaTime;
			}
		}

		for (int i = 0; i < notifications.Length; ++i) {
			while (notifications[i] != null && notifications[i].duration < 0) {
				for (int j = i; j < notifications.Length - 1; ++j) {
					notifications[j] = notifications[j+1];
				}
				notifications[notifications.Length - 1] = null;
			}
			if (notifications[i] != null) {
				text += notifications[i].text + "\n";
			}
		}

		textStyle.fontSize = (int)(28 * scaleFactor);
		GUI.Label (new Rect(19 * scaleFactor, 200 * scaleFactor, 500 * scaleFactor, 300 * scaleFactor), text, textStyle);
	}
}
