﻿using UnityEngine;
using System.Collections;

public class ProjectileComponent : MonoBehaviour {
	
	public string prefabId;
	public Unit target;
	public Unit attacker;
	public Vector3 attackerStartPosition;
	public Vector3 targetStartPosition;

	public virtual void Setup(Unit attacker, Unit target) {
		this.attacker = attacker;
		this.target = target;
		if (attacker != null && !attacker.isDead) {
			attackerStartPosition = attacker.gameObject.transform.position;
		}
		if (target != null && !target.isDead) {
			targetStartPosition = target.gameObject.transform.position;
		}
	}

	public virtual void Setup(Vector3 from, Vector3 to) {
		attackerStartPosition = from;
		targetStartPosition = to;
	}
}
