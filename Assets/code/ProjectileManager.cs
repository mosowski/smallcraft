﻿using UnityEngine;
using System.Collections;

public static class ProjectileManager {

	public class ProjectileManagerPool : Pool {
		public override GameObject Construct(string prefabId) {
			GameObject obj = Object.Instantiate(Resources.Load<GameObject>("projectiles/" + prefabId)) as GameObject;
			return obj;
		}
	}

	public static ProjectileManagerPool pool = new ProjectileManagerPool();

	public static void CreateProjectile(string prefabId, Unit attacker, Unit target) {
		pool.Pick(prefabId).GetComponent<ProjectileComponent>().Setup(attacker, target);
	}

	public static void CreateProjectile(string prefabId, Vector3 from, Vector3 to) {
		pool.Pick(prefabId).GetComponent<ProjectileComponent>().Setup(from, to);
	}

	public static void ReleaseProjectile(GameObject obj) {
		pool.Release(obj);
	}
}
