﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum TerrainType {
	High,
	Low,
	Water
}

public enum FoWState {
	Visible,
	Visited,
	Hidden
}

public class MapTile : AStar.Node {
	public float height;
	public List<Unit> units;
	public TerrainType terrainType;
	public FoWState fowState;
	public float[] rampVertices;

	public MapTile(int col, int row) : base() {
		x = col;
		y = row;
		units = new List<Unit>();
		terrainType = TerrainType.Water;
		fowState = FoWState.Hidden;
	}

	public void SetRampVertices(float angle, int segmentCol, int segmentRow, float min, float max) {
		rampVertices = new float[4];
		int roundedAngle = (int)(Mathf.Round(angle/90.0f));
		if (roundedAngle == 0) {
			if (segmentCol == 1) {
				min = max/2;
			} else {
				max /= 2;
			}
			rampVertices[0] = rampVertices[2] = min;
			rampVertices[1] = rampVertices[3] = max;
		} else if (roundedAngle == 1) {
			if (segmentRow == 0) {
				min = max/2;
			} else {
				max /= 2;
			}
			rampVertices[2] = rampVertices[3] = min;
			rampVertices[0] = rampVertices[1] = max;
		} else if (roundedAngle == 2) {
			if (segmentCol == 0) {
				min = max/2;
			} else {
				max /= 2;
			}
			rampVertices[1] = rampVertices[3] = min;
			rampVertices[0] = rampVertices[2] = max;
		} else {
			if (segmentRow == 1) {
				min = max/2;
			} else {
				max /= 2;
			}
			rampVertices[0] = rampVertices[1] = min;
			rampVertices[2] = rampVertices[3] = max;
		}
	}

	// 23
	// 01
	public float GetHeight(float localX, float localY) {
		if (rampVertices != null) {
			float h0 = localX * rampVertices[1] + (1 - localX) * rampVertices[0];
			float h1 = localX * rampVertices[3] + (1 - localX) * rampVertices[2];
			float h = localY * h1 + (1 - localY) * h0;
			return h;
		} else {
			return height;
		}
	}

	public int Minerals {
		get {
			int sum = 0;
			foreach (Unit u in units) {
				if (u is Mineral) {
					sum += (u as Mineral).minerals;
				}
			}
			return sum;
		}
	}

	public Mineral FirstMineral {
		get {
			foreach (Unit u in units) {
				if (u is Mineral) {
					return u as Mineral;
				}
			}
			return null;
		}
	}
}
