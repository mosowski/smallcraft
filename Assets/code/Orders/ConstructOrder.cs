﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConstructOrder : MoveOrder {
	public int column;
	public int row;
	public UnitModel constructionModel;

	public ConstructOrder(Unit unit, string modelId, int column, int row) : base(unit) {
		this.column = column;
		this.row = row;
		constructionModel = unit.player.model.GetModel(modelId);
		SetTargetPoint(Game.GetTilePosition(column, row));
	}

	protected override void OnRechedDestination() {
		if ((unit.player.gold >= constructionModel.cost) 
			&& (Game.CanPlaceBuilding(column, row, constructionModel))) {
			unit.player.gold -= constructionModel.cost;
			Factory.MakeBuilding(constructionModel.id, column, row, unit.player);
		} else {
			Notifications.AddLocal(unit.player, "Cannot build here");
		}
		unit.CancelOrder();
	}
}
