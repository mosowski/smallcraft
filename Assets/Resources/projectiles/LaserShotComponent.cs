﻿using UnityEngine;
using System.Collections;

public class LaserShotComponent : ProjectileComponent {

	public float timeToLive;

	LineRenderer lineRenderer;

	public override void Setup(Unit attacker, Unit target) {
		base.Setup(attacker, target);
		lineRenderer = GetComponent<LineRenderer>();
		lineRenderer.SetPosition(0, attackerStartPosition + Vector3.up);
		lineRenderer.SetPosition(1, targetStartPosition);
		timeToLive = 0.2f;
	}

	void Update() {
		timeToLive -= Time.deltaTime;
		lineRenderer.SetPosition(0, attackerStartPosition + (targetStartPosition - attackerStartPosition) * (1.0f - timeToLive / 0.2f));
		if (timeToLive <= 0) {
			ProjectileManager.ReleaseProjectile(gameObject);
		}
	}
}
