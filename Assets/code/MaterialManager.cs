﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class MaterialManager {

	public static Dictionary<string, Material> materials = new Dictionary<string, Material>();
	public static List<Color> playerColors = new List<Color>() { 
		Color.red, 
		new Color(0, 0.5f, 0.5f, 1), 
		new Color(0.5f, 0, 0.5f, 1),
	   	Color.yellow
	};

	public static Material GetMaterialForPlayer(Material material, Player player) {
		string name = material.name + player.number;
		if (materials.ContainsKey(name)) {
			return materials[name];
		} else {
			Material newMaterial = Object.Instantiate(material) as Material;
			if (newMaterial.HasProperty("_DecalColor")) {
				newMaterial.SetColor("_DecalColor", playerColors[player.number]);
			} else {
				newMaterial.SetColor("_Color", playerColors[player.number]);
			}
			newMaterial.name = name;
			materials[name] = newMaterial;
			return newMaterial;
		}
	}

}
