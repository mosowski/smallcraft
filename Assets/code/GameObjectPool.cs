﻿using UnityEngine;
using System.Collections;

public static class GameObjectPool {

	public class _GameObjectPool : Pool {
		public override GameObject Construct(string prefabId) {
			GameObject obj = Object.Instantiate(Resources.Load<GameObject>(prefabId)) as GameObject;
			return obj;
		}
	}

	public static _GameObjectPool pool = new _GameObjectPool();

	public static GameObject Create(string prefabId, GameObject parent = null) {
		GameObject obj = pool.Pick(prefabId);
		if (parent != null) {
			obj.transform.parent = parent.transform;
		}
		return obj;
	}

	public static void Release(GameObject obj) {
		pool.Release(obj);
	}
}
