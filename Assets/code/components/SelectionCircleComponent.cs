﻿using UnityEngine;
using System.Collections;

public class SelectionCircleComponent : MonoBehaviour {
	public Unit unit;

	void Update () {
		if (unit.isSelected) {
			transform.position += (new Vector3(unit.position.x, unit.Tile.height, unit.position.y) - transform.position) * 5.0f * Time.deltaTime;
		}
	}
}
