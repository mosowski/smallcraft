using UnityEngine;
using System.Collections;

public class UnitProductionQueueItem : ProductionQueueItem {

	protected UnitModel model;

	public UnitProductionQueueItem(Unit producer, string itemName) : base(producer, itemName) {
		model = producer.player.model.GetModel(itemName);
	}

	public override void OnCompleted() {
		base.OnCompleted();
		producer.player.usedSupply -= model.supplyCost;

		Vector2 summonPosition = producer.SummonPosition;
		if (summonPosition != Vector2.zero) {
			Unit unit = Factory.MakeUnit(model.id, summonPosition, producer.player);

			Game.AcknowledgeUnitConstructed(producer.player, unit);
		} else {
			Notifications.AddLocal(producer.player, "No room for " + model.displayName + " !");
		}
	}

	public override void OnQueued() {
		base.OnQueued();
		producer.player.usedSupply += model.supplyCost;
	}

	public override void Cancel() {
		base.Cancel();
		producer.player.usedSupply -= model.supplyCost;
	}
}
