﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {
	
	public class AudioManagerPool : Pool {
		public override GameObject Construct(string prefabId) {
			GameObject obj = new GameObject();
			AudioSource audio = obj.AddComponent<AudioSource>();
			obj.AddComponent<SFXComponent>();
			audio.clip = Resources.Load<AudioClip>("sounds/" + prefabId);
			audio.volume = 1.0f;
			audio.pitch = 1.0f;
			audio.dopplerLevel = 0;
			audio.playOnAwake = false;
			return obj;
		}
	}

	public static AudioManagerPool pool = new AudioManagerPool();

	public static GameObject PlaySFX(string prefabId, Vector3 position, bool loop = false) {
		GameObject obj = pool.Pick(prefabId);
		obj.GetComponent<AudioSource>().loop = loop;
		obj.GetComponent<AudioSource>().Play();
		obj.GetComponent<SFXComponent>().Setup(loop);
		obj.transform.position = Camera.main.transform.position + (position - Camera.main.transform.position) * 0.2f;
		return obj;
	}

	public static void ReleaseSFX(GameObject obj) {
		pool.Release(obj);
	}
}

public class SFXComponent : MonoBehaviour {
	float timeToLive;

	public void Setup(bool loop) {
		if (!loop) {
			timeToLive = GetComponent<AudioSource>().clip.length + 0.1f;
		} else {
			timeToLive = -1;
		}
	}

	void Update() {
		if (timeToLive > 0) {
			timeToLive -= Time.deltaTime;
			if (timeToLive <= 0) {
				AudioManager.ReleaseSFX(gameObject);
			}
		}
	}
}

