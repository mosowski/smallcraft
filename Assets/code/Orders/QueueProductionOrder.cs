﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QueueProductionOrder : Order {

	public QueueProductionOrder(Unit unit, string itemName) : base(unit) {
		unit.QueueProduction(itemName);
	}
}

