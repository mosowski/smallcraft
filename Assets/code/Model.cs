﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Model {
	
	private Dictionary<string,UnitModel> models;
	public string[] buildingModelIds;

	public Model() {
		models = new Dictionary<string, UnitModel>();

		buildingModelIds = new string[] { "CommandCenter", "Greenhouse", "Barracs", "Turret", "Factory", "ResearchStation", "Hangar" };

		CreateMineral();
		CreateCommandCenter();
		CreateGreenhouse();
		CreateBarracs();
		CreateTurret();
		CreateFactory();
		CreateHangar();
		CreateResearchStation();

		CreateWorker();
		CreateMarine();
		CreateTrooper();
		CreateSpider();
		CreateATV();
		CreateTrike();
		CreateShuttle();
		CreateRaptor();
		CreateBigMech();
	}

	public UnitModel GetModel(string name) {
		return models[name];
	}

	public IEnumerable<string> ModelIds {
		get {
			return models.Keys;
		}
	}

	private void CreateMineral() {
		UnitModel model = new UnitModel();
		model.id = "Mineral";
		model.displayName = "Minerals";
		model.clickRadius = 0.85f;
		model.smallRadius = 0.55f;
		model.maxHp = 40;
		model.armor = 7;
		model.isIndestructible = true;

		model.features = new List<Feature.Params>();
		models[model.id] = model;
	}

	private void CreateWorker() {
		UnitModel model = new UnitModel();
		model.id = "Worker";
		model.displayName = "Worker";
		model.constructionTime = 100;
		model.clickRadius = 0.85f;
		model.smallRadius = 0.35f;
		model.sightRange = 7;
		model.isFlying = false;
		model.maxHp = 40;
		model.armor = 0;

		model.cost = 50;
		model.supplyCost = 1;

		model.destuctionPrefab = "SmallExplosion";

		model.features = new List<Feature.Params>() {
			new GatherFeature.Params(),
			new AttackFeature.Params() {
				groundDamage = 2,
				groundRange = 3.0f,
				groundShotSound = "laser",
				groundProjectile = "LaserShot",
				cooldown = 10
			},
			new ProductionFeature.Params() {
				producedUnits = buildingModelIds
			},
			new MoveFeature.Params() {
				maxSpeed = 0.125f,
				inertia = 0.8f
			}
		};

		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateMarine() {
		UnitModel model = new UnitModel();
		model.id = "Marine";
		model.displayName = "Marine";
		model.constructionTime = 120;
		model.maxSpeed = 0.125f;
		model.inertia = 0.8f;
		model.clickRadius = 0.85f;
		model.smallRadius = 0.45f;
		model.sightRange = 7;
		model.maxHp = 40;
		model.armor = 0;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.canAttackAir = true;
		model.projectilePrefab = "LaserShot";
		model.attackCooldown = 20;
		model.attackRange = 6.0f;
		model.damage = 6;
		model.shootSound = "machineGun";
		*/

		model.cost = 50;
		model.supplyCost = 1;

		model.destuctionPrefab = "BloodExplosion";

		model.features = new List<Feature.Params>();
		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateTrooper() {
		UnitModel model = new UnitModel();
		model.id = "Trooper";
		model.displayName = "Trooper";
		model.constructionTime = 150;
		model.maxSpeed = 0.115f;
		model.inertia = 0.8f;
		model.clickRadius = 0.85f;
		model.smallRadius = 0.45f;
		model.sightRange = 7;
		model.maxHp = 55;
		model.armor = 0;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.projectilePrefab = "RocketProjectile";
		model.attackCooldown = 25;
		model.attackRange = 6.5f;
		model.damage = 11;
		model.damageDelay = 9;
		model.splashRange = 2;
		*/

		model.cost = 80;
		model.supplyCost = 1;

		model.destuctionPrefab = "BloodExplosion";

		model.features = new List<Feature.Params>();
		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateSpider() {
		UnitModel model = new UnitModel();
		model.id = "Spider";
		model.displayName = "Virus";
		model.constructionTime = 200;
		model.maxSpeed = 0.205f;
		model.inertia = 0.8f;
		model.clickRadius = 1.55f;
		model.smallRadius = 1.05f;
		model.sightRange = 7;
		model.maxHp = 150;
		model.armor = 1;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.projectilePrefab = "LaserShot";
		model.attackCooldown = 12;
		model.attackRange = 7.0f;
		model.damage = 10;
		model.shootSound = "bigLaser";
		*/

		model.cost = 160;
		model.supplyCost = 2;

		model.destuctionPrefab = "SmallExplosion";

		model.features = new List<Feature.Params>();
		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateTrike() {
		UnitModel model = new UnitModel();
		model.id = "Trike";
		model.displayName = "Trike";
		model.constructionTime = 150;
		model.maxSpeed = 0.300f;
		model.inertia = 0.8f;
		model.clickRadius = 1.05f;
		model.smallRadius = 0.55f;
		model.sightRange = 7;
		model.maxHp = 90;
		model.armor = 0;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.projectilePrefab = "LaserShot";
		model.attackCooldown = 10;
		model.attackRange = 5.0f;
		model.damage = 4;
		model.shootSound = "laser";
		*/

		model.cost = 100;
		model.supplyCost = 2;

		model.destuctionPrefab = "SmallExplosion";

		model.features = new List<Feature.Params>();
		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateATV() {
		UnitModel model = new UnitModel();
		model.id = "ATV";
		model.displayName = "ATV";
		model.constructionTime = 220;
		model.maxSpeed = 0.160f;
		model.inertia = 0.8f;
		model.clickRadius = 1.25f;
		model.smallRadius = 0.75f;
		model.sightRange = 7;
		model.maxHp = 200;
		model.armor = 1;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.projectilePrefab = "TankShot";
		model.attackCooldown = 25;
		model.attackRange = 8.0f;
		model.damage = 25;
		model.splashRange = 2;
		*/

		model.cost = 200;
		model.supplyCost = 2;

		model.destuctionPrefab = "SmallExplosion";

		model.features = new List<Feature.Params>();
		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateShuttle() {
		UnitModel model = new UnitModel();
		model.id = "Shuttle";
		model.displayName = "Shuttle";
		model.constructionTime = 200;
		model.maxSpeed = 0.225f;
		model.inertia = 0.3f;
		model.clickRadius = 1.25f;
		model.smallRadius = 0.45f;
		model.sightRange = 10;
		model.isFlying = true;
		model.flyingHeight = 1.5f;
		model.maxHp = 120;
		model.armor = 0;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.projectilePrefab = "LaserShot";
		model.attackCooldown = 10;
		model.attackRange = 6.0f;
		model.damage = 7;
		*/

		model.cost = 120;
		model.supplyCost = 2;

		model.destuctionPrefab = "SmallExplosion";

		model.features = new List<Feature.Params>();
		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateRaptor() {
		UnitModel model = new UnitModel();
		model.id = "Raptor";
		model.displayName = "Raptor";
		model.constructionTime = 150;
		model.maxSpeed = 0.285f;
		model.inertia = 0.3f;
		model.clickRadius = 1.25f;
		model.smallRadius = 0.45f;
		model.sightRange = 10;
		model.isFlying = true;
		model.flyingHeight = 1.6f;
		model.maxHp = 90;
		model.armor = 0;

		/*
		model.isArmed = true;
		model.canAttackAir = true;
		model.projectilePrefab = "RocketProjectile";
		model.attackCooldown = 20;
		model.attackRange = 6.0f;
		model.damage = 30;
		model.damageDelay = 9;
		*/

		model.cost = 110;
		model.supplyCost = 2;

		model.destuctionPrefab = "SmallExplosion";

		model.features = new List<Feature.Params>();

		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateBigMech() {
		UnitModel model = new UnitModel();
		model.id = "BigMech";
		model.displayName = "Schmetterling";
		model.constructionTime = 300;
		model.maxSpeed = 0.075f;
		model.inertia = 0.8f;
		model.clickRadius = 1.2f;
		model.smallRadius = 0.9f;
		model.sightRange = 10;
		model.maxHp = 400;
		model.armor = 3;

		model.cost = 500;
		model.supplyCost = 4;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.canAttackAir = true;
		model.projectilePrefab = "RocketProjectile";
		model.attackCooldown = 25;
		model.attackRange = 8.0f;
		model.damage = 30;
		model.damageDelay = 9;
		model.splashRange = 3;
		*/

		model.destuctionPrefab = "BigExplosion";

		model.features = new List<Feature.Params>();

		model.acknowledegeSounds = new List<string>() { };

		models[model.id] = model;
	}

	private void CreateCommandCenter() {
		UnitModel model = new UnitModel();
		model.id = "CommandCenter";
		model.displayName = "Command center";
		model.constructionTime = 1000;
		model.clickRadius = 2.2f;
		model.smallRadius = 2.0f;
		model.sightRange = 10;
		model.maxHp = 1500;
		model.armor = 0;
		model.tileWidth = 4;
		model.tileHeight = 4;
		model.destuctionPrefab = "BigExplosion";

		model.providedSupply = 10;
		model.cost = 400;

		model.features = new List<Feature.Params>() {
			new ProductionFeature.Params() {
				producedUnits = new[]{ "Worker" }
			}
		};

		models[model.id] = model;
	}

	private void CreateGreenhouse() {
		UnitModel model = new UnitModel();
		model.id = "Greenhouse";
		model.constructionTime = 300;
		model.displayName = "Greenhouse";
		model.clickRadius = 1.2f;
		model.smallRadius = 1.0f;
		model.sightRange = 6;
		model.maxHp = 400;
		model.armor = 0;
		model.tileWidth = 2;
		model.tileHeight = 2;
		model.destuctionPrefab = "BigExplosion";

		model.providedSupply = 10;
		model.cost = 100;

		model.productionDependencies = new List<string>() { "CommandCenter" };

		model.features = new List<Feature.Params>();

		models[model.id] = model;
	}

	private void CreateTurret() {
		UnitModel model = new UnitModel();
		model.id = "Turret";
		model.constructionTime = 200;
		model.displayName = "Turret";
		model.clickRadius = 1.2f;
		model.smallRadius = 1.0f;
		model.sightRange = 12;
		model.maxHp = 200;
		model.armor = 1;
		model.destuctionPrefab = "BigExplosion";

		model.tileWidth = 2;
		model.tileHeight = 2;
		model.isDefensiveConstruction = true;

		model.cost = 125;

		/*
		model.isArmed = true;
		model.canAttackGround = true;
		model.canAttackAir = true;
		model.projectilePrefab = "RocketProjectile";
		model.attackCooldown = 20;
		model.attackRange = 10.0f;
		model.damage = 15;
		model.damageDelay = 9;
		*/

		model.productionDependencies = new List<string>() { "Barracs" };

		model.features = new List<Feature.Params>();

		models[model.id] = model;
	}

	private void CreateBarracs() {
		UnitModel model = new UnitModel();
		model.id = "Barracs";
		model.constructionTime = 500;
		model.displayName = "Barracs";
		model.clickRadius = 1.8f;
		model.smallRadius = 1.5f;
		model.sightRange = 6;
		model.maxHp = 1000;
		model.armor = 0;
		model.tileWidth = 3;
		model.tileHeight = 3;
		model.destuctionPrefab = "BigExplosion";

		model.cost = 150;

		model.productionDependencies = new List<string>() { "CommandCenter" };

		model.features = new List<Feature.Params>();

		models[model.id] = model;
	}

	private void CreateFactory() {
		UnitModel model = new UnitModel();
		model.id = "Factory";
		model.constructionTime = 500;
		model.displayName = "Factory";
		model.clickRadius = 1.8f;
		model.smallRadius = 1.5f;
		model.sightRange = 6;
		model.maxHp = 1000;
		model.armor = 0;
		model.tileWidth = 3;
		model.tileHeight = 3;
		model.destuctionPrefab = "BigExplosion";

		model.cost = 220;

		model.productionDependencies = new List<string>() { "Barracs" };

		model.features = new List<Feature.Params>();

		models[model.id] = model;
	}

	private void CreateHangar() {
		UnitModel model = new UnitModel();
		model.id = "Hangar";
		model.constructionTime = 500;
		model.displayName = "Hangar";
		model.clickRadius = 1.8f;
		model.smallRadius = 1.5f;
		model.sightRange = 6;
		model.maxHp = 800;
		model.armor = 0;
		model.tileWidth = 3;
		model.tileHeight = 3;
		model.destuctionPrefab = "BigExplosion";

		model.cost = 175;

		model.productionDependencies = new List<string>() { "Factory" };

		model.features = new List<Feature.Params>();

		models[model.id] = model;
	}

	private void CreateResearchStation() {
		UnitModel model = new UnitModel();
		model.id = "ResearchStation";
		model.constructionTime = 500;
		model.displayName = "Research Station";
		model.clickRadius = 2.2f;
		model.smallRadius = 2.0f;
		model.sightRange = 7;
		model.maxHp = 1100;
		model.armor = 0;
		model.tileWidth = 4;
		model.tileHeight = 4;
		model.destuctionPrefab = "BigExplosion";

		model.cost = 200;

		model.productionDependencies = new List<string>() { "Barracs" };

		model.features = new List<Feature.Params>();

		models[model.id] = model;
	}

	public bool IsBuilding(string modelName) {
		return models[modelName].tileWidth != 0;
	}

	public string GetProductionDependenciesListed(UnitModel model) {
		string result = "";
		foreach (string dependency in model.productionDependencies) {
			if (result != "") {
				result += ", ";
			}
			result += models[dependency].displayName;
		}
		return result;
	}
}
