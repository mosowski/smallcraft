﻿using UnityEngine;
using System.Collections;

public class Order {

	public Unit unit;

	public Order(Unit unit) {
		this.unit = unit;
	}

	public virtual void Tick() {
	}

	public virtual void Begin() {
	}

	public virtual void Cancel() {
	}
}
