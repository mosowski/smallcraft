﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool {
	public Dictionary<string, Stack<GameObject>> pools = new Dictionary<string, Stack<GameObject>>();

	public virtual GameObject Construct(string prefabId) {
		return null;
	}

	public GameObject Pick(string prefabId) { 
		Stack<GameObject> pool;
		if (!pools.ContainsKey(prefabId)) {
			pool = pools[prefabId] = new Stack<GameObject>();
		} else {
			pool = pools[prefabId];
		}

		GameObject obj = null;

		if (pool.Count > 0) {
			obj = pool.Pop();
			obj.SetActive(true);
		} else {
			obj = Construct(prefabId);
			obj.name = prefabId;
		}
		return obj;
	}

	public void Release(GameObject obj) {
		pools[obj.name].Push(obj);
		obj.SetActive(false);
	}

}
