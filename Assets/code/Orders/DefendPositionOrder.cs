﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DefendPositionOrder : Order {

	AttackFeature attackFeature;

	public DefendPositionOrder(Unit unit): base(unit) {
		attackFeature = unit.GetFeature<AttackFeature>();
	}

	public override void Tick() {
		if (attackFeature.targetUnit != null && !attackFeature.targetUnit.isDead) {
			if (attackFeature.CanShoot(attackFeature.targetUnit)) {
				attackFeature.Shoot(attackFeature.targetUnit);
			} else {
				attackFeature.targetUnit = null;
			}
		} else {
			attackFeature.targetUnit = attackFeature.FindTargetInRange();
		}
	}

}

