﻿using UnityEngine;
using System.Collections;

public class CommonAssets {

	public static Texture healthBarBg;
	public static Texture healthBarFg;
	public static Texture productionBarFg;

	public static Font label_font;

	public static Texture2D minimap;

	public static Texture 
		UIBG, UITop,
		UI_ActionMoveInactive, UI_ActionMoveActive,
		UI_ActionAttackInactive, UI_ActionAttackActive,
		UI_ActionSpecialInactive, UI_ActionSpecialActive,
		UI_ActionStopInactive, UI_ActionStopActive,
		UI_ActionGatherInactive, UI_ActionGatherActive,
		UI_ActionBuildInactive, UI_ActionBuildActive,
		UI_MobStats;
	public static Texture
		MenuBG, MenuButton, MenuButtonWide,
		MenuFrameGray, MenuFrameYellow, MenuFrameGraySmall,
		Logo, Victory, Defeat, RoomStaticLabel;
	public static GUIStyle
		GUIButtonStyle, GUIButtonWideStyle, GUIJoinButtonStyle,
		GUITextfieldStyle, LogoStyle, RoomStyle, RoomSizeStyle,
		GUIFrameYellow, GUIPlayerStyle,
		GUIIconStyle;
	public static GUIStyle
		OrderMoveActive, OrderMoveInactive,
		OrderAttackActive, OrderAttackInactive,
		OrderAttackMoveActive, OrderAttackMoveInactive,
		OrderStopActive, OrderStopInactive,
		OrderGatherActive, OrderGatherInactive,
		OrderBuildActive, OrderBuildInactive;
	public static AudioClip
		GUIButtonClick, GUIButtonHover, GUIMusic;
	public static float 
		UIBG_minHeight = 216, 
		UIBG_topHeight = 320,
		UIBG_minLeft = 425, 
		UIBG_minRight = 1366;

	public static void Init() {
		healthBarBg = Resources.Load<Texture>("2d/healthBarBg");
		healthBarFg = Resources.Load<Texture>("2d/healthBarFg");
		productionBarFg = Resources.Load<Texture>("2d/productionBarFg");

		UIBG = Resources.Load<Texture>("2d/UI");
		UITop = Resources.Load<Texture>("2d/UITop");
		UI_ActionMoveActive = Resources.Load<Texture>("2d/moveActive");
		UI_ActionMoveInactive = Resources.Load<Texture>("2d/moveInactive");
		UI_ActionAttackActive = Resources.Load<Texture>("2d/attackActive");
		UI_ActionAttackInactive = Resources.Load<Texture>("2d/attackInactive");
		UI_ActionSpecialActive = Resources.Load<Texture>("2d/specialActive");
		UI_ActionSpecialInactive = Resources.Load<Texture>("2d/specialInactive");
		UI_ActionStopActive = Resources.Load<Texture>("2d/stopActive");
		UI_ActionStopInactive = Resources.Load<Texture>("2d/stopInactive");
		UI_ActionGatherActive = Resources.Load<Texture>("2d/gatherActive");
		UI_ActionGatherInactive = Resources.Load<Texture>("2d/gatherInactive");
		UI_ActionBuildActive = Resources.Load<Texture>("2d/buildActive");
		UI_ActionBuildInactive = Resources.Load<Texture>("2d/buildInactive");
		UI_MobStats = Resources.Load<Texture>("2d/mobStats");

		label_font = Resources.Load<Font>("Arial");

		MenuBG = Resources.Load<Texture2D>("2d/menu_bg");
		MenuButton = Resources.Load<Texture>("2d/smallcraft_button");
		MenuButtonWide = Resources.Load<Texture>("2d/smallcraft_button_wide");
		MenuFrameGray = Resources.Load<Texture>("2d/smallcraft_frame_grey");
		MenuFrameYellow = Resources.Load<Texture>("2d/smallcraft_frame_yellow");
		MenuFrameGraySmall = Resources.Load<Texture>("2d/smallcraft_frame_grey_small");
		Logo = Resources.Load<Texture>("2d/smallcraft_logo");
		Victory = Resources.Load<Texture>("2d/smallcraft_victory");
		Defeat = Resources.Load<Texture>("2d/smallcraft_defeat");
		RoomStaticLabel = Resources.Load<Texture>("2d/smallcraft_frame_yellow_small");

		GUIMusic = Resources.Load<AudioClip>("sounds/dzwiek_menu");
		GUIButtonClick = Resources.Load<AudioClip>("sounds/klikniecie_przycisku");

		GUIIconStyle = new GUIStyle();
		GUIStyle basic = new GUIStyle();
		basic.font = label_font;
		basic.fontSize = 14;
		basic.normal.textColor = new Color(0x21 / 255f, 0x21 / 255f, 0x21 / 255f);
		basic.alignment = TextAnchor.MiddleCenter;

		GUIButtonStyle = new GUIStyle(basic);
		GUIButtonStyle.normal.background = (Texture2D)MenuButton;
		GUIButtonStyle.fixedWidth = MenuButton.width;
		GUIButtonStyle.fixedHeight = MenuButton.height;
		GUIButtonStyle.fontStyle = FontStyle.Bold;

		GUIJoinButtonStyle = new GUIStyle(GUIButtonStyle);
		GUIJoinButtonStyle.margin = new RectOffset(0,5,4,7);

		GUIButtonWideStyle = new GUIStyle(GUIButtonStyle);
		GUIButtonWideStyle.normal.background = (Texture2D)MenuButtonWide;
		GUIButtonWideStyle.fixedWidth = MenuButtonWide.width;
		GUIButtonWideStyle.fixedHeight = MenuButtonWide.height;

		GUITextfieldStyle = new GUIStyle(basic);
		GUITextfieldStyle.normal.background = (Texture2D)MenuFrameGray;
		GUITextfieldStyle.fixedWidth = MenuFrameGray.width;
		GUITextfieldStyle.fixedHeight = MenuFrameGray.height;
		GUITextfieldStyle.normal.textColor = Color.white;

		GUIPlayerStyle = new GUIStyle(GUITextfieldStyle);
		GUIPlayerStyle.margin = new RectOffset(0,0,0,10);

		GUIFrameYellow = new GUIStyle(basic);
		GUIFrameYellow.normal.background = (Texture2D)MenuFrameYellow;
		GUIFrameYellow.fixedWidth = MenuFrameYellow.width;
		GUIFrameYellow.fixedHeight = MenuFrameYellow.height;
		GUIFrameYellow.normal.textColor = Color.white;

		LogoStyle = new GUIStyle();
		LogoStyle.margin = new RectOffset(0,0,200,100);

		RoomStyle = new GUIStyle(GUIFrameYellow);
		RoomStyle.margin = new RectOffset(0,5,7,7);
		RoomStyle.alignment = TextAnchor.LowerCenter;

		RoomSizeStyle = new GUIStyle(basic);
		RoomSizeStyle.normal.background = (Texture2D)MenuFrameGraySmall;
		RoomSizeStyle.fixedWidth = MenuFrameGraySmall.width;
		RoomSizeStyle.fixedHeight = MenuFrameGraySmall.height;
		RoomSizeStyle.normal.textColor = Color.white;



		OrderMoveActive = new GUIStyle();
		OrderMoveActive.normal.background = (Texture2D)UI_ActionMoveActive;
		OrderMoveActive.hover.background = (Texture2D)UI_ActionMoveActive;
		OrderMoveInactive = new GUIStyle(OrderMoveActive);
		OrderMoveInactive.normal.background = (Texture2D)UI_ActionMoveInactive;

		OrderAttackActive = new GUIStyle();
		OrderAttackActive.normal.background = (Texture2D)UI_ActionAttackActive;
		OrderAttackActive.hover.background = (Texture2D)UI_ActionAttackActive;
		OrderAttackInactive = new GUIStyle(OrderAttackActive);
		OrderAttackInactive.normal.background = (Texture2D)UI_ActionAttackInactive;

		OrderAttackMoveActive = new GUIStyle();
		OrderAttackMoveActive.normal.background = (Texture2D)UI_ActionSpecialActive;
		OrderAttackMoveActive.hover.background = (Texture2D)UI_ActionSpecialActive;
		OrderAttackMoveInactive = new GUIStyle(OrderAttackMoveActive);
		OrderAttackMoveInactive.normal.background = (Texture2D)UI_ActionSpecialInactive;

		OrderStopActive = new GUIStyle();
		OrderStopActive.normal.background = (Texture2D)UI_ActionStopActive;
		OrderStopActive.hover.background = (Texture2D)UI_ActionStopActive;
		OrderStopInactive = new GUIStyle(OrderStopActive);
		OrderStopInactive.normal.background = (Texture2D)UI_ActionStopInactive;

		OrderGatherActive = new GUIStyle();
		OrderGatherActive.normal.background = (Texture2D)UI_ActionGatherActive;
		OrderGatherActive.hover.background = (Texture2D)UI_ActionGatherActive;
		OrderGatherInactive = new GUIStyle(OrderGatherActive);
		OrderGatherInactive.normal.background = (Texture2D)UI_ActionGatherInactive;

		OrderBuildActive = new GUIStyle();
		OrderBuildActive.normal.background = (Texture2D)UI_ActionBuildActive;
		OrderBuildActive.hover.background = (Texture2D)UI_ActionBuildActive;
		OrderBuildInactive = new GUIStyle(OrderBuildActive);
		OrderBuildInactive.normal.background = (Texture2D)UI_ActionBuildInactive;
	}

	public static void updateSizes()
	{
		float factor = (float)Camera.main.pixelWidth / (float)UIBG.width;

		GUIIconStyle.fixedWidth = 81 * factor;
		GUIIconStyle.fixedHeight = 81 * factor;
		GUIIconStyle.border = new RectOffset((int)(10 * factor), (int)(10 * factor), (int)(10 * factor), (int)(10 * factor));

		OrderMoveActive.fixedWidth = 90 * factor;
		OrderMoveActive.fixedHeight = 90 * factor;
		OrderMoveInactive.fixedWidth = 90 * factor;
		OrderMoveInactive.fixedHeight = 90 * factor;

		OrderAttackActive.fixedWidth = 90 * factor;
		OrderAttackActive.fixedHeight = 90 * factor;
		OrderAttackInactive.fixedWidth = 90 * factor;
		OrderAttackInactive.fixedHeight = 90 * factor;

		OrderAttackMoveActive.fixedWidth = 90 * factor;
		OrderAttackMoveActive.fixedHeight = 90 * factor;
		OrderAttackMoveInactive.fixedWidth = 90 * factor;
		OrderAttackMoveInactive.fixedHeight = 90 * factor;


		OrderStopActive.fixedWidth = 90 * factor;
		OrderStopActive.fixedHeight = 90 * factor;
		OrderStopInactive.fixedWidth = 90 * factor;
		OrderStopInactive.fixedHeight = 90 * factor;

		OrderGatherActive.fixedWidth = 90 * factor;
		OrderGatherActive.fixedHeight = 90 * factor;
		OrderGatherInactive.fixedWidth = 90 * factor;
		OrderGatherInactive.fixedHeight = 90 * factor;

		OrderBuildActive.fixedWidth = 90 * factor;
		OrderBuildActive.fixedHeight = 90 * factor;
		OrderBuildInactive.fixedWidth = 90 * factor;
		OrderBuildInactive.fixedHeight = 90 * factor;


	}
}
