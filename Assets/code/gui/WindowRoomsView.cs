﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindowRoomsView : MonoBehaviour {

	GameObject roomsList;
	GameObject scrollView;

	List<GameObject> rooms = new List<GameObject>();
	void Start () {
		scrollView = transform.Find("RoomsList").gameObject;
		roomsList = scrollView.transform.Find("Grid").gameObject;
		Lobby.RoomsReceived += OnRoomsChange;
		Lobby.EnteredRoom += OnRoomEnter;
		Lobby.RoomFull += OnRoomFull;
		UIEventListener.Get(transform.Find("RefreshButton").gameObject).onClick += OnRefreshButtonClick;
		UIEventListener.Get(transform.Find("RankingButton").gameObject).onClick += OnRankingButtonClick;
		Lobby.GetRooms();
	}

	void OnRankingButtonClick(GameObject go) {
		AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
		GUIManager.OpenWindow("WindowRanking");
		Close();
	}

	void OnRefreshButtonClick(GameObject go) {
		AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
		Lobby.GetRooms();
	}

	void OnCreateRoomButtonClick(GameObject go) {
		string roomName = transform.Find("CreateRoomInput").GetChild(0).GetComponent<UIInput>().value;
		Debug.Log("asd" + roomName);
		JoinRoom(roomName);
	}

	void JoinRoom(string roomName) {
		AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
		Lobby.JoinRoom(roomName);
	}

	void ClearRooms() {
		foreach(GameObject go in rooms) {
			NGUITools.Destroy(go);
		}
		rooms.Clear();
	}

	void Close() {
		Lobby.RoomsReceived -= OnRoomsChange;
		Lobby.EnteredRoom -= OnRoomEnter;
		Lobby.RoomFull -= OnRoomFull;
		GUIManager.CloseWindow("WindowRooms");
	}

	void OnRoomEnter() {
		GUIManager.OpenWindow("WindowRoom");
		Close();	
	}


	void OnRoomsChange() {
		ClearRooms();
		UIGrid grid = roomsList.GetComponent<UIGrid>();
		foreach(RoomInfo room in Lobby.rooms) {
			GameObject window = NGUITools.AddChild(roomsList, Resources.Load<GameObject>("gui/Room"));
			window.transform.localPosition = new Vector3(0, 0, 0);
			window.transform.Find("RoomName").Find("Text").GetComponent<UILabel>().text = room.name;
			window.transform.Find("RoomSize").Find("Text").GetComponent<UILabel>().text = room.players + "/" + room.maxPlayers;
			string roomName = room.name;
			UIEventListener.Get(window.transform.Find("JoinButton").gameObject).onClick += (go) => {
				JoinRoom(roomName);
			};
			rooms.Add(window);
		}
		grid.Reposition();
		scrollView.GetComponent<UIScrollView>().ResetPosition();
	}

	void OnRoomFull() {
		transform.Find("Status").GetComponent<UILabel>().text = "Room full";
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.Return)) {
			OnCreateRoomButtonClick(null);
		}
	}
}
