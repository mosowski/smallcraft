﻿using UnityEngine;
using System.Collections;

public class UnitComponent : BaseUnitComponent {

	public override void Setup(Unit unit) {
		base.Setup(unit);

		DestroyBody();
		body = GameObjectPool.Create("units/body/" + unit.model.id);
		AttachBody();
		SetBodyMaterial();

		SetPositionImmediately();
		IsVisible = false;
	}

	protected virtual void UpdateUnit() {
		if (!unit.isDead) {
			if (unit.Tile.fowState == FoWState.Visible) {
				if (!IsVisible) {
					IsVisible = true;
					SetPositionImmediately();
				}
				UpdatePosition();
				UpdateDirection();
			} else {
				if (IsVisible) {
					IsVisible = false;
				}
			}
		}
	}

	void Update () {
		UpdateSelectionMarker();
		UpdateUnit();
		UpdateGameObject();
	}
}
