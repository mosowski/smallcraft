using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class GatherOrder : Order {
	public GatherFeature gatherFeature;

	public GatherOrder(Unit unit, Vector2 targetPoint) : base(unit) {
		gatherFeature = unit.GetFeature<GatherFeature>();
		gatherFeature.targetPoint = targetPoint;
		gatherFeature.depositUnit = Game.GetNearestDepositUnit(unit.player, targetPoint);

		if (gatherFeature.depositUnit != null) {
			FindDepositPoint();

			gatherFeature.mineral = Game.GetNearestMineral(gatherFeature.targetPoint);

		}
	}

	public override void Begin() {
		unit.SetOrder(new SideFindOrder(unit, this));
	}

	public void ValidateDepositPoint() {
		if (!Game.IsWalkable(Game.GetMapTile(gatherFeature.depositPoint))) {
			FindDepositPoint();
		}
	}

	public void FindDepositPoint() {
		MapTile depositTile = Game.GetNearestDepositTile(gatherFeature.depositUnit, gatherFeature.targetPoint);
		gatherFeature.depositPoint = new Vector2(depositTile.x - 0.5f, depositTile.y - 0.5f);
	}


	public class SideFindOrder : MoveOrder {
		GatherOrder parentOrder;

		public SideFindOrder(Unit unit, GatherOrder parentOrder) : base(unit, parentOrder.gatherFeature.mineral.position) {
			this.parentOrder = parentOrder;
		}

		public override void Tick() {
			base.Tick();
		}

		protected override void OnRechedDestination() {
			unit.SetOrder(new SideGatherOrder(unit, parentOrder));
		}
	}

	public class SideGatherOrder : Order {
		GatherOrder parentOrder;
		GatherFeature gatherFeature;
		MoveFeature moveFeature;

		public SideGatherOrder(Unit unit, GatherOrder parentOrder): base(unit) {
			this.parentOrder = parentOrder;
			gatherFeature = parentOrder.gatherFeature;
			moveFeature = unit.GetFeature<MoveFeature>();
		}

		int gatherSpeed = 1;
		int maxAmountGathered = 7;
		int tickToGather = 10;

		int gatheredTicks = 0;
		bool isGathering;

		public override void Tick() {
			moveFeature.isCollidable = false;
			unit.headDirection = gatherFeature.mineral.position - unit.position;

			if (!isGathering && gatherFeature.mineral.CanGather) {
				gatherFeature.mineral.AddWorker();
				isGathering = true;
			}

			if (isGathering) {
				gatheredTicks++;
				unit.isGathering = true;
				if (gatheredTicks == tickToGather) {
					int gathered = Mathf.Min(gatherSpeed, gatherFeature.mineral.minerals);
					gatherFeature.mineral.minerals -= gathered;
					gatherFeature.amountGathered += gathered;

					gatheredTicks = 0;
					if (gatherFeature.amountGathered >= maxAmountGathered) {
						OnFull();
					}
					if (gatherFeature.mineral.minerals  <= 0) {
						OnResourceDepleted();
					}
				}
			}
		}

		protected virtual void OnResourceDepleted() {
			DepositResources();
		}

		protected virtual void OnFull() {
			DepositResources();
		}

		private void DepositResources() {
			isGathering = false;

			gatherFeature.mineral.RemoveWorker();

			parentOrder.ValidateDepositPoint();
			if (!gatherFeature.depositUnit.isDead) {
				if (gatherFeature.amountGathered > 0) {
					unit.carriedObject = GameObjectPool.Create("misc/MineralMini", unit.gameObject);
					unit.carriedObject.transform.localPosition = new Vector3(-0.5f, 0.2f, 0);
				}

				unit.SetOrder(new SideDepositOrder(unit, parentOrder));
			} else {
				unit.CancelOrder();
			}
		}

		public override void Cancel() {
			if (isGathering) {
				gatherFeature.mineral.RemoveWorker();
			}
			moveFeature.isCollidable = true;
			unit.isGathering = false;
			base.Cancel();
		}
	}

	public class SideDepositOrder : MoveOrder {
		GatherOrder parentOrder;
		GatherFeature gatherFeature;

		public SideDepositOrder(Unit unit, GatherOrder parentOrder) : base(unit, parentOrder.gatherFeature.depositPoint) {
			this.parentOrder = parentOrder;
			gatherFeature = parentOrder.gatherFeature;
		}

		private void RemoveMineralFromHands() {
			if (unit.carriedObject != null) {
				GameObjectPool.Release(unit.carriedObject);
				unit.carriedObject = null;
			}
		}

		protected override void OnRechedDestination() {
			unit.player.gold += gatherFeature.amountGathered;
			gatherFeature.amountGathered = 0;

			RemoveMineralFromHands();

			gatherFeature.mineral = Game.GetNearestMineral(gatherFeature.targetPoint);

			if (gatherFeature.mineral != null) {
				gatherFeature.targetPoint = gatherFeature.mineral.position;
				unit.SetOrder(new SideFindOrder(unit, parentOrder));
			} else {
				unit.CancelOrder();
			}
		}

		public override void Cancel() {
			base.Cancel();
			RemoveMineralFromHands();
		}
	}
}

