﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindowRoomView : MonoBehaviour {

	List<string> names = new List<string>();

	List<GameObject> items = new List<GameObject>();

	// Use this for initialization
	void Start () {
		Lobby.RankingReceived += Init;
		Lobby.PlayerJoinedRoom += OnPlayerJoinedRoom;
		Lobby.PlayerLeftRoom += OnPlayerLeftRoom;
		Lobby.LeftRoom += OnLeftRoom;
		Lobby.GameStarted += OnGameStarted;
		UIEventListener.Get(transform.Find("LeaveButton").gameObject).onClick += (go) => {
			AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
			Lobby.LeaveRoom();
		};

		UIEventListener.Get(transform.Find("StartButton").gameObject).onClick += (go) => {
			AudioSource.PlayClipAtPoint(CommonAssets.GUIButtonClick, Camera.main.transform.position);
			Root.client.Call("start");
		};
		Lobby.GetRanking();
	}

	void OnGameStarted() {
		Close();
	}

	void OnPlayerJoinedRoom(string roomName, string playerName) {
		if (roomName == Lobby.CurrentRoom.name) {
			names.Add(playerName);
			Redraw();
		}
	}

	void OnPlayerLeftRoom(string roomName, string playerName) {
		if (roomName == Lobby.CurrentRoom.name) {
			names.Remove(playerName);
			Redraw();
		}
	}

	void OnLeftRoom() {
		Close();
		GUIManager.OpenWindow("WindowRooms");
	}

	void Close() {
		Lobby.RankingReceived -= Init;
		Lobby.PlayerJoinedRoom -= OnPlayerJoinedRoom;
		Lobby.PlayerLeftRoom -= OnPlayerLeftRoom;
		Lobby.LeftRoom -= OnLeftRoom;
		Lobby.GameStarted -= OnGameStarted;
		GUIManager.CloseWindow("WindowRoom");
	}
	
	void Init() {
		foreach(string name in Lobby.CurrentRoom.members) {
			names.Add(name);
		}
		Redraw();
	}

	void Redraw() {
		foreach(GameObject item in items) {
			NGUITools.Destroy(item);
		}
		items.Clear();

		GameObject list = transform.Find("RoomList").Find("Grid").gameObject;
		foreach (string name in names) {
			GameObject window = NGUITools.AddChild(list, Resources.Load<GameObject>("gui/RankingRow"));
			RankingItem ri = Lobby.GetRankingByPlayerName(name);
			window.transform.Find("Name").Find("Text").GetComponent<UILabel>().text = name;
			window.transform.Find("Score").Find("Text").GetComponent<UILabel>().text = ri != null ? ri.score.ToString() : "--";
			items.Add(window);
		}
		list.GetComponent<UIGrid>().Reposition();
		transform.Find("RoomList").GetComponent<UIScrollView>().ResetPosition();
		transform.Find("RoomLabel").Find("Text").GetComponent<UILabel>().text = "Room: " + Lobby.CurrentRoom.name + " [" + names.Count.ToString() + "/" + Lobby.CurrentRoom.maxPlayers + "]";
	}

	// Update is called once per frame
	void Update () {
	
	}
}
