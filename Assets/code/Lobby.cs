﻿using UnityEngine;
using System;
using System.Collections;
using LitJson;

public class RoomInfo {
	public string name;
	public int players;
	public int maxPlayers;
	public string[] members;
	public string ownerName;
}

public class RankingItem {
	public string name;
	public int score; 
}

public class Lobby {

	public static string playerName { get; private set; }
	public static string roomName { get; private set; }

	public static int playerRank;
	public static RankingItem[] ranking;

	public static RoomInfo[] rooms;
	public static int numPlayersOnline;

	public static event Action<string, string> PlayerJoinedRoom = (roomName, playerName) => {};
	public static event Action<string, string> PlayerLeftRoom = (roomName, playerName) => {};
	public static event Action RoomFull = () => {};
	public static event Action EnteredRoom = () => {};
	public static event Action LeftRoom = () => {};
	public static event Action RankingReceived = () => {};
	public static event Action LoginOk = () => {};
	public static event Action<string> LoginFailed = (error) => {};
	public static event Action RoomsReceived = () => {};
	public static event Action GameStarted = () => {};

	public static void Init() {
		rooms = new RoomInfo[0];
	}

	public static void LogIn(string name, string password) {
		Root.client.Call("logIn", name, password);
		playerName = name;
	}

	public static void GetRooms() {
		Root.client.Call("getRooms");
		Root.client.Call("getGlobalStats");
	}

	public static void JoinRoom(string name) {
		Root.client.Call("joinRoom", name);
		Root.client.Call("getRooms");
	}

	public static void LeaveRoom() {
		Root.client.Call("leaveRoom", playerName);
	}

	public static void GetRanking() {
		Root.client.Call("getRanking");
	}

	public static void OnGameStarted() {
		GameStarted();
	}
	
	public static void OnLoginOk() {
		LoginOk();
		Root.client.Call("getGlobalStats");
	}

	public static void OnLoginFailed(string error) {
		playerName = "";
		LoginFailed(error);
	}

	public static void OnSetRooms(JsonData data) {
		rooms = new RoomInfo[data[0].Count];

		for(int i = 0; i < data[0].Count; i++) {
			RoomInfo room = new RoomInfo();
			room.name = (string)data[0][i]["roomName"];
			//room.ownerName = (string)data[0][i]["ownerName"];
			room.players = data[0][i]["members"].Count;
			room.maxPlayers = (int)data[0][i]["roomSize"];

			room.members = new string[room.players];
			for(int j = 0; j < room.players; j++) {
				room.members[j] = (string)data[0][i]["members"][j];
			}
			rooms[i] = room;
		}
		RoomsReceived();
		
	}

	public static void OnPlayerJoinedRoom(string roomName, string playerName) {
		if (playerName == Lobby.playerName) {
			Lobby.roomName = roomName;
			EnteredRoom();
		}
		PlayerJoinedRoom(roomName, playerName);
	}

	public static void OnPlayerLeftRoom(string roomName, string playerName) {
		if (playerName == Lobby.playerName) {
			Lobby.roomName = "";
			LeftRoom();
		} else {
			PlayerLeftRoom(roomName, playerName);
		}
	}

	public static void OnRoomFull() {
		RoomFull();
	}

	public static void OnRankingReceived(JsonData data) {
		playerRank = (int)data["rank"];
		ranking = new RankingItem[data["ranking"].Count];
		for (int i = 0; i < ranking.Length; ++i) {
			ranking[i] = new RankingItem() {
				name = (string)data["ranking"][i]["player"],
				score = int.Parse((string)data["ranking"][i]["score"])
			};
		}
		RankingReceived();
	}

	public static RankingItem GetRankingByPlayerName(string name) {
		foreach (RankingItem ri in ranking) {
			if (ri.name == name) {
				return ri;
			}
		}
		return null;
	}

	public static RoomInfo GetRoomByName(string name) {
		foreach (RoomInfo ri in rooms) {
			if (ri.name == name) {
				return ri;
			}
		}
		return null;
	}

	public static RoomInfo CurrentRoom {
		get {
			return GetRoomByName(roomName);
		}
	}

}
