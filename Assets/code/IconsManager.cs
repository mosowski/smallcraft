using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IconsManager {
	public static Dictionary<string, Texture2D> cache = new Dictionary<string, Texture2D>();

	
	public static Texture2D GetSmallIcon(string modelId) {
		string key = modelId + "Small";
		if (cache.ContainsKey(key)) {
			return cache[key];
		} else {
			return cache[key] = Resources.Load<Texture2D>("2d/units/small/"+modelId);
		}
	}

	public static Texture2D GetBigIcon(string modelId) {
		string key = modelId + "Big";
		if (cache.ContainsKey(key)) {
			return cache[key];
		} else {
			return cache[key] = Resources.Load<Texture2D>("2d/units/big/"+modelId);
		}
	}

}
